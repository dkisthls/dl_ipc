;+
; NAME:
;       dlnirsp_ipc_summsg
;
; PURPOSE:
;       A routine to produce a nicely formatted input and output summary.
;
; CALLING SEQUENCE:
; 
;
; INPUTS:
;
; OPTIONAL INPUTS:
;
; KEYWORD PARAMETERS:
;       None 
;
; OUTPUTS:
;       
; OPTIONAL OUTPUTS:
;
; COMMON BLOCKS:
;       dlnirsp_environment
;
; PROCEDURES USED:
;
; COMMENTS:
;
; EXAMPLES:
;
; MODIFICATION HISTORY:
;       Started 2017-Apr-27 by Sarah A. Jaeggli, National Solar
;               Observatory
;               2022-05-24 SAJ made major changes, see note in dlnirsp_ipc.pro
;               2024-03-11 SAJ made some corrections and updates for MISIs
;------------------------------------------------------------------------------

FUNCTION time_unit, time_in, mult
  ;; input time in seconds, convert to easy to read units
  mult=1.
  text='sec'
  
  if time_in gt 60. then begin
     mult=1./60.
     text='min'
  endif
  
  if time_in gt 3600. then begin
     mult=1./3600.
     text='hr '
  endif

  if time_in gt 3600.*24. then begin
     mult=1./3600./24.
     text='day'
  endif
    
  return, text
END

;;-----------------------------------------------------------------------------
  
FUNCTION data_unit, data_in, mult
  ;; input data in MB, convert to easy to read units
  mult=1.0
  text='MB'

  if data_in gt 1e3 then begin
     mult=1e-3
     text='GB'
  endif
  
  if data_in gt 1e6 then begin
     mult=1e-6
     text='TB'
  endif

  if data_in gt 1e9 then begin
     mult=1e-9
     text='PB'
  endif
  
  return, text
END

;;-----------------------------------------------------------------------------

PRO dlnirsp_ipc_summsg, msgin, msgout
  
  COMMON dlnirsp_environment, input, output, tp, atlas

  wave=[input.wave1[input.selected.wave1], $
        input.wave2[input.selected.wave2], $
        input.wave3[input.selected.wave3]]

  cam=[input.cam1, input.cam2, input.cam3]
  scat=input.inten.scat.val[input.selected.scat]
  
  wave_sel=[input.selected.wave1, input.selected.wave2, input.selected.wave3]
  
  good_wave=where(wave_sel gt 0, nwave)
  
  wave=wave[good_wave]
  cam=cam[good_wave]

  sep='------------------------------------------------------------'
  
  
;;--Input Text-----------------------------------------------------------------
  loc_str=['Off Disk,  r='+string(output.target.robs, format='(F4.2)'), $
           ' On Disk, mu='+string(output.target.mu, format='(F4.2)')]
  
  msgin=[sep, 'Target: ' + loc_str[output.target.ondisk], $ 
         ' Center of Field (X,Y) [arcsec]: ' + $
         string(input.target.x, input.target.y, format='(I4,",",I4)'), $
         '           Coude Rotation [deg]: '+ $
         string(input.target.angle, format='(F5.1)')]

  
  msgin=[msgin, $
          sep, 'Intensities:  [I/I(Disk Center)]', $
         '   Scattered Light : ' + $
         string(input.inten.scat.val[input.selected.scat], format='(E7.1)'), $
         '                        Arm 1,    Arm 2,    Arm 3']
  
  if output.target.ondisk ne 1 then $
     msgin=[msgin, $
            '              Line : ' + $
            string(input.inten.line[0].i, format='(E8.2,", ")') + $
            string(input.inten.line[1].i, format='(E8.2,", ")') + $
            string(input.inten.line[2].i, format='(E8.2)')]
  
  msgin=[msgin, $
         '         Continuum : ' + $
         string(input.inten.cont[0], format='(E8.2,", ")') + $
         string(input.inten.cont[1], format='(E8.2,", ")') + $
         string(input.inten.cont[2], format='(E8.2)')]

  
  msgin=[msgin, $
         sep, 'Telescope: ', $
         ' Telescope Diameter [cm]: '+ $
         string(input.tele.diam, format='(F5.1)')]

  
  msgin=[msgin, $       
         sep, 'Feed: ' + input.feed[input.selected.feed].name, $
         '           Focal Ratio [f/#]: ' + $
         string(input.feed[input.selected.feed].fr, format='(I2)'), $
         '     Plate Scale [arcsec/um]: ' + $
         string(input.feed[input.selected.feed].ps, format='(E8.2)'),$
         ' Max. Field of View [arcsec]: ' + $
         string(input.feed[input.selected.feed].fov, format='(F5.1)')]

  msgin=[msgin, $         
         sep, 'IFU: '+input.ifu[input.selected.ifu].name, $
         ' Number X Elements: ' + $
         string(input.ifu[input.selected.ifu].iput.nrib, format='(I3)'), $
         ' Number Y Elements: ' + $
         string(input.ifu[input.selected.ifu].iput.nstk * $
                input.ifu[input.selected.ifu].fiber.nfbr[1], format='(I3)'), $
         ' Element Dimensions X,Y [um]: ' + $
         string(input.ifu[input.selected.ifu].fiber.x + $
                2. * input.ifu[input.selected.ifu].fiber.c, $
                input.ifu[input.selected.ifu].fiber.y + $
                2. * input.ifu[input.selected.ifu].fiber.c, $
                format='(I5,", ",I5)'), $
         '                    [arcsec]: ' + $
         string(output.signal[good_wave[0]].dx, $
                output.signal[good_wave[0]].dy_fib, $
                format='(F5.3,", ",F5.3)')]

  msgin=[msgin, $
         sep, 'Mosaic: ', $
         '           Tile Size (X,Y) [arcsec]: ' + $
         string(output.mosaic.tilex, output.mosaic.tiley, $
                format='(F7.2,", ",F7.2)'), $
         '            Mosaic Dimensions (X,Y): ' + $
         string(input.mosaic.nx, input.mosaic.ny, format='(I7,", ",I7)'), $
         ' Total Field of View (X,Y) [arcsec]: ' + $
         string(output.mosaic.fovx, output.mosaic.fovy, $
                format='(F7.2,", ",F7.2)'), $
         '          Time to Step Mosaic [sec]: ' + $
         string(input.mosaic.move_time, format='(F5.3)')]


    msgin=[msgin, $
           sep, 'Spectrograph: ']
    
    for i=0,nwave-1 do $
       msgin=[msgin, 'Arm '+string(good_wave[i]+1, format='(I1)')+':', $
              '            Center Wavelength [nm]: ' + $
              string(output.wave[good_wave[i]].l0, format='(F7.2)'), $
              '                    Bandwidth [nm]: ' + $
              string(output.wave[good_wave[i]].ran[1] - $
                     output.wave[good_wave[i]].ran[0], format='(F4.2)'), $
              '    Spectral Dispersion [pm/pixel]: ' + $
              string(output.wave[good_wave[i]].dl*1e3, format='(F4.2)'), $
              ' Spatial Dispersion [arcsec/pixel]: ' + $
              string(output.signal[good_wave[i]].dy_cam, format='(F5.3)')]

    
    msgin=[msgin, sep, 'Cameras: ']

    for i=0,nwave-1 do $
       msgin=[msgin, 'Camera '+string(good_wave[i]+1, format='(I1)') + ': ' + $
              cam[i].name, $
              '        Exposure Time [sec]: ' + $
              string(cam[i].exp, '(F8.3)'), $
              '                     # NDRs: ' + $
              string(cam[i].ndr, $
                     format='(I4)'), $
              '                  # Co-Adds: ' + $
              string(cam[i].coadd, '(I4)'), $
              '     Pixel Dimensions [x,y]: ' + $
              string(cam[i].nx, cam[i].ny, $
                     format='(I4,",",I4)'), $
              '            Pixel Size [um]: ' + string(cam[i].pitch, $
                                                       format='(F5.1)'), $
              '            Frame Rate [Hz]: ' + string(cam[i].fr, $
                                                       format='(F6.2)'), $
              '              Gain [e-/ADU]: ' + string(cam[i].gain, $
                                                       format='(F6.2)'), $
              '            Read Noise [e-]: ' + string(cam[i].rn, $
                                                       format='(F6.2)'), $
              '      Dark Current [e-/sec]: ' + string(cam[i].dc, $
                                                       format='(E8.2)'), $
              'Thermal Background [e-/sec]: ' + string(cam[i].tb, $
                                                       format='(E8.2)'), $
              '           Bias Offset [e-]: ' + string(cam[i].bi, $
                                                       format='(E8.2)'), $
              '             Bits per Pixel: ' + $
              string(cam[i].bits_per_pixel, format='(I2)')]


      msgin=[msgin, $
             sep,'Polarimetry: ', $
             '         # Accumulation Cycles: ' + $
             string(input.pol.accum, format='(I4)'), $
             '                    Modulation: ' + $
             input.pol.mode[input.selected.pol], $
             '              Number of States: ' + $
             string(input.pol.nstates, format='(I4)'), $
             '  Time to Step Modulator [sec]: ' + $
             string(input.pol.move_time * $
                    (0 > (input.selected.pol-1) < 1), format='(F5.3)')]
 

;;--Output Text----------------------------------------------------------------
  warning='Warnings:'
  
  ;; check_field of view
  dlnirsp_ipc_check_fov, warning
  
  if input.selected.fido eq 0 and input.selected.wave1 ne 0 then $
     warning=[warning, 'Visible channel is running at low efficiency with this FIDO beam splitter configuration.']

  if total(output.signal.sat) gt 0 then warning=[warning, $
     'One or more channels have reached the full well capacity: ', $
     wave[where(output.signal.sat eq 1)].name]
  
  if input.selected.feed eq 2 and output.target.ondisk eq 1 then $
     warning=[warning, 'Wide field coronal mode is being used on disk.']

  if input.selected.feed lt 2 and output.target.ondisk eq 0 then $
     warning=[warning, 'High resolution modes are being used off disk.  Use wide mode for higher count rates.']
  
;  if input.selected.feed lt 2 and input.selected.ifu eq 1 then $
;     warning=[warning, 'IFU-72 is being used without the coronal lens.  This setup is experimental.']

;  if input.selected.feed eq 2 and input.selected.ifu eq 0 then $
;     warning=[warning, 'IFU-36 is being used with the coronal lens.  This setup is experimental.']

  
  ;; check if diagnostics are appropriate
  if output.target.ondisk then begin
     bad=where(wave.type eq 'co', count)
     if count ge 1 then $
        warning=[warning, 'Coronal diagnostics are being used on-disk.']
  endif else begin
     bad=where(wave.type eq 'ph', count)
     if count ge 1 then $
        warning=[warning,'Photospheric diagnostics are being used off-disk.']
  endelse

  ;; check data rate
;  if output.data.rate gt 192. then $
;     warning=[warning, 'It is advised to keep total DL-NIRSP data rates below 192 MB/sec when co-operating with ViSP, VTF, and/or VBI in order to stay within facility data limits.  Increase minimum SNR or exposure times/coadds to reduce the cadence.']


  if output.target.ondisk eq 1 then $
     SNR_str='----------> Stokes I Continuum SNR: ' $
  else $
     SNR_str='---------------> Stokes I Line SNR: '

  
  msg_ch1='Channel 1: '+input.wave1[input.selected.wave1].name
  if input.selected.wave1 gt 0 then begin
     i=0
     msg_ch1=[msg_ch1, $

              '         Instrument Efficiency [%]: ' + $
              string(output.signal[i].eff*100., format='(F5.1)'), $

              'Total Light Signal [photons/pixel]: ' + $
              string(output.signal[i].phot + $
                     output.signal[i].back, format='(E8.2)'), $
              
              '    Science Signal [photons/pixel]: ' + $
              string(output.signal[i].phot, format='(E8.2)'),$

              '                       % Full-well: ' + $
              string((output.signal[i].phot + $
                      output.signal[i].back + $
                      output.signal[i].dark) / $
                     input.cam1.fw*100., format='(F6.0)'), $

              SNR_str + $
              string(output.signal[i].phot / $
                     output.noise[i].total, format='(I5)'), $

              '        Diffraction Limit [arcsec]: ' + $
              string(output.signal[i].diff, format='(F5.3)'), $

              '            Spectral Sampling [pm]: ' + $
              string(output.signal[i].dl*1e3, format='(F4.2)')]
     
     spat_samp=output.signal[i].dx
  endif
  
  msg_ch2='Channel 2: '+input.wave2[input.selected.wave2].name
  if input.selected.wave2 gt 0 then begin
     i=1
     msg_ch2=[msg_ch2, $

              '         Instrument Efficiency [%]: ' + $
              string(output.signal[i].eff*100., format='(F5.1)'), $

              'Total Light Signal [photons/pixel]: ' + $
              string(output.signal[i].phot + $
                     output.signal[i].back, format='(E8.2)'), $
              
              '    Science Signal [photons/pixel]: ' + $
              string(output.signal[i].phot, format='(E8.2)'), $

              '                       % Full-well: ' + $
              string((output.signal[i].phot + $
                      output.signal[i].back + $
                      output.signal[i].dark) / $
                     input.cam2.fw*100., format='(F6.0)'), $

              SNR_str + $
              string(output.signal[i].phot / $
                     output.noise[i].total, format='(I5)'), $

              '        Diffraction Limit [arcsec]: ' + $
              string(output.signal[i].diff, format='(F5.3)'), $

              '            Spectral Sampling [pm]: ' + $
              string(output.signal[i].dl*1e3, format='(F4.2)')]
     
     spat_samp=output.signal[i].dx
  endif
    
  msg_ch3='Channel 3: '+input.wave3[input.selected.wave3].name
  if input.selected.wave3 gt 0 then begin
     i=2
     msg_ch3=[msg_ch3, $

              '         Instrument Efficiency [%]: ' + $
              string(output.signal[i].eff*100., format='(F5.1)'), $

              'Total Light Signal [photons/pixel]: ' + $
              string(output.signal[i].phot + $
                     output.signal[i].back, format='(E8.2)'), $
              
              '    Science Signal [photons/pixel]: ' + $
              string(output.signal[i].phot, format='(E8.2)'),$

              '                       % Full-well: ' + $
              string((output.signal[i].phot + $
                      output.signal[i].back + $
                      output.signal[i].dark) / $
                     input.cam3.fw*100., format='(F6.0)'), $

              SNR_str + $
              string(output.signal[i].phot / $
                     output.noise[i].total, format='(I5)'), $
              
              '        Diffraction Limit [arcsec]: ' + $
              string(output.signal[i].diff, format='(F5.3)'), $                

              '            Spectral Sampling [pm]: ' + $
              string(output.signal[i].dl*1e3, format='(F4.2)')]
     
     spat_samp=output.signal[i].dx
  endif

  loc_str=['Coronal','Disk']
  
  msgout=[warning, sep, $
          
          '                            Target: '+loc_str[output.target.ondisk], $
          
          'Total Field of View (X,Y) [arcsec]: ' + $
          string(output.mosaic.fovx, output.mosaic.fovy, $
                 format='(F7.1,",",F7.1)'), $
          
          '         Spatial Sampling [arcsec]: ' + $
          string(spat_samp, format='(F5.3)'), $
          
          sep, $
          msg_ch1, sep, $
          msg_ch2, sep, $
          msg_ch3, sep, $

          '  per Tile // Time ['+time_unit(output.time.tile,mult)+']: ' + $\
          string(output.time.tile*mult, format='(F8.2)') + $
          '  Data ['+data_unit(output.data.tile, mult)+']: ' + $
          string(output.data.tile*mult, format='(F8.2)'), $
          
          'per Mosaic // Time ['+time_unit(output.time.mosaic,mult)+']: ' + $
          string(output.time.mosaic*mult, format='(F8.2)') + $
          '  Data ['+data_unit(output.data.mosaic,mult)+']: ' + $
          string(output.data.mosaic*mult, format='(F8.2)'), $
          
          '     Total // Time ['+time_unit(output.time.obs,mult)+']: ' + $
          string(output.time.obs*mult, format='(F8.2)') + $
          '  Data ['+data_unit(output.data.obs,mult)+']: ' + $
          string(output.data.obs*mult, format='(F8.2)'), $
           
          '','      Data Rate ['+data_unit(output.data.rate,mult)+'/sec]: ' + $
          string(output.data.rate*mult, format='(F8.2)')]
  
end
