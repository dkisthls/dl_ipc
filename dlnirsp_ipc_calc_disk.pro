; Calculate the intensity of the quiet-Sun continuum as a function of
; radial distance and wavelength
;+
; NAME:
;       dlnirsp_ipc_calc_disk
;
; PURPOSE:
;       A routine to produce a nicely formatted input and output summary.
;
; CALLING SEQUENCE:
;       result = dlnirsp_ipc_calc_disk(wave, mu, /cgs)
;
; INPUTS:
;       wave - wavelength in [nm], can be an array
;       mu - cos(theta), should be a scalar
;
; OPTIONAL INPUTS:
;
; KEYWORD PARAMETERS:
;       cgs 
;
; OUTPUTS:
;       returns photons / sec / cm^2 / nm / str   or  if the cgs
;       keyword is set it returns erg / sec / cm^2 / nm / str
;
; OPTIONAL OUTPUTS:
;
; COMMON BLOCKS:
;
; PROCEDURES USED:
;
; COMMENTS:
;
; EXAMPLES:
;
; MODIFICATION HISTORY:
;       Started 2017-Apr-27 by Sarah A. Jaeggli, National Solar Observatory
;------------------------------------------------------------------------------

function dlnirsp_ipc_calc_disk, wave, mu, cgs=cgs
  
  ;; Allen's Astrophysical Quantities (2000)
  ;;     w0 - wavelength in microns  
  ;;     i0 - values taken from I_lambda^prime(0) column on page 353.
  ;;          "intensity of the center of the Sun's disk between spectral 
  ;;          lines.  This is obtained by interpolation from the most 
  ;;          intense windows."
  
  ;; wavelength in [microns]
  w0=[0.20, 0.22, 0.24, 0.26, 0.28, 0.30, 0.32, 0.34, 0.36, 0.37, $
      0.38, 0.39, 0.40, 0.41, 0.42, 0.43, 0.44, 0.45, 0.46, 0.48, $
      0.50, 0.55, 0.60, 0.65, 0.70, 0.75, 0.80, 0.90, 1.00, 1.10, $
      1.20, 1.40, 1.60, 1.80, 2.00, 2.50, 3.00, 4.00, 5.00]*1000.
  
  ;; central intensity in [W m-2 sr-1 Ang-1]
  i0=[0.02, 0.19, 0.21, 0.53, 1.21, 2.39, 2.94, 3.30, 3.47, 3.60, $
      4.14, 4.41, 4.58, 4.63, 4.66, 4.67, 4.62, 4.55, 4.44, 4.22, $
      4.08, 3.63, 3.24, 2.90, 2.52, 2.24, 1.97, 1.58, 1.26, 1.01, $
      0.84, 0.560, 0.40, 0.27, 0.18, 0.081, 0.041, 0.014, 0.006]*1e3

  ;; interpolate the Allen table over wavelength to get the average
  ;; continuum intensity at disk center
  i0=interpol(i0, w0, wave)

  
  ;; Allen's Astrophysical Quantities (2000)
  ;;     w0 - wavelength in microns  
  ;;     u2,v2 - the limb darkening coefficients

  w0 = [0.20, 0.22, 0.245, 0.265, 0.280, 0.30, 0.32, 0.35, 0.37, $
        0.38, 0.40, 0.45, 0.50, 0.55, 0.60, 0.80, 1.0, 1.5, 2.0, $
        3.0, 5.0, 10.0]*1000.
  u2 = [0.12,-1.30, -0.1, -0.1, 0.38 ,0.74 ,0.88 , 0.98, 1.03, $
        0.92, 0.91, 0.99, 0.97, 0.93, 0.88, 0.73, 0.64, 0.57, $
        0.48, 0.35, 0.22, 0.15]
  v2 = [0.33, 1.60, 0.85 , 0.90 ,0.57 ,0.20 ,0.03 ,-0.10,-0.16, $
        -0.05, -0.05,-0.17,-0.22,-0.23,-0.23,-0.22,-0.20,-0.21, $
        -0.18,-0.12,-0.07,-0.07]

  ;; calculate the normalized limb darkening function for the particular
  ;; value of mu  
  imu = 1. - u2 - v2 + u2*mu + v2*mu^2
  
  ;; and interpolate to the correct wavelength
  imu=interpol(imu, w0, wave)


  ;; put the central intensity and limb darkening together to get the
  ;; absolute intensity at the desired wavelengths, and convert to units
  ;; of photons s-1 sr-1 cm-2 nm-1
  output=imu * $
         i0  * $                            ; [J s-1 m-2 sr-1 Ang-1]
         1e-4 * $                           ; m-2 to cm-2
         10. * $                            ; Ang-1 to nm-1
         (wave*1e-9) / (6.626069e-34 * 3e8) ; photons / energy =
                                            ; lambda [m] / h [J sec] * c [m/s]
  
  if keyword_set(cgs) eq 1 then $
     output = output * (6.626069e-27 * 3e8) / (wave*1e-9)
  
  return, output

end
