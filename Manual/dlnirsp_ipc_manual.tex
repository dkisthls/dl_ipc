\documentclass{article}

\usepackage{natbib}
\usepackage{hyperref}
\usepackage{graphicx}

\title{Manual for the DL-NIRSP Instrument Performance Calculator}
\author{Sarah A. Jaeggli}

\begin{document}

\maketitle

\tableofcontents

\section{Brief Description of the Instrument}
The Diffraction-Limited Infrared Spectropolarimeter (DL-NIRSP) is an integral field instrument which covers specific diagnostics at visible and near-infrared wavelengths.  The baseline wavelengths will include [Fe XIV] 530.3 nm, He I D3 587.6 nm, Fe I 630.2 nm, [Fe XI] 789.2 nm, Ca II 854.2 nm, [Fe XIII] 1074.7 nm, [Fe XIII] 1079.8, He I and Si I 1083.0 nm, [Si X] 1430.0 nm, and Fe I 1565.0 nm.  Three spectral arms with independent optics and detectors allow for simultaneous coverage of multiple diagnostics at once, one from each waveband 500-900, 900-1350, and 1350-1800 nm.  DL-NIRSP has feed optics which can provide different focal ratios in order to optimize resolution, field of view coverage, or signal to noise for on-disk or coronal observations.  The small field of view of the fiber-optic integral field unit can be tiled to create mosaics of larger areas.  DL-NIRSP is situated at the end of the facility instrument distribution optics (FIDO), where it can take advantage of the adaptive-optics corrected beam, and observe simultaneously with other visible wavelength instruments.

The critical quantity for the calculation of instrument performance is the signal to noise ratio (SNR).  This calculator estimates the signal to noise for a calibrated and spatially resampled data product.  For the on-disk case the calculator estimates the signal to noise of the unpolarized quiet-Sun continuum at the particular wavelength of interest.  For coronal emission lines, the signal to noise is taken for the unpolarized line emission at its peak amplitude.  Additional considerations are necessary to determine polarimetric sensitivity.  For example, if a line is 10\% polarized then the polarimetric signal to noise will be SNR*10\%.


\section{Getting Started}
The DL-NIRSP Instrument Performance Calculator (IPC) is written in IDL using widgets.  You will need to unpack the files in your directory of choice.  You can run IDL from inside the directory or add the directory to your IDL path.

\vspace{12pt}
Start the IPC from IDL with:

\noindent \verb+IDL> dlnirsp_ipc+
\vspace{12pt}

\noindent This will produce two windows shown in Figure \ref{fig:default}, one with the user-selectable input, the other with output text summaries and example spectra.  The basic idea is that you will set the user-selectable input, hit the calculate button, then look at the resulting summaries.  You should be able to fiddle with it without looking at the rest of this document, but if you really want to, there is an exhaustive description of each part of the widget.

Note: The current version of the IPC has been pre-configured for the DKIST Cycle 2 proposal call.  Please see the call for more details on the instrument configuration.  If there are discrepancies, defer to the description in the proposal call.
\begin{figure}
	\begin{center}
		\includegraphics[width=5in]{./figures/input_window0a.png}
		\includegraphics[width=1.67in]{./figures/output_window0.png}
	\end{center}
	\caption{The input and output windows as they appear at startup.}
	\label{fig:default}
\end{figure}

\clearpage


\section{Example Observations}
The following sections show a few examples of what can be done with the IPC on the disk, at the limb, and above the limb.

\subsection{Detailed Flux Emergence}
This is an example of a setup you might use for studying magnetic fields at high resolution.  In this example, DL-NIRSP gets just the IR, assuming coordination with other instruments.  Reasonable exposure levels were first determined using the inverse calculation tab, and then adjusted using the forward calculation tab.

\begin{figure}
	\begin{center}
		\includegraphics[width=5in]{./figures/input_window1.png}
		\includegraphics[width=1.67in]{./figures/output_window1a.png}\includegraphics[width=1.67in]{./figures/output_window1b.png}\includegraphics[width=1.67in]{./figures/output_window1c.png}
	\end{center}
	\caption{The calculator windows as they appear for the example of a study of detailed flux emergence.}
	\label{fig2}
\end{figure}


\subsection{Spicules}
Here's something you might use to study spicules at the limb.  Just to show that the mosaics can be long and thin, slit-like but better than a slit because you have some spatial coverage to each side.  In this case, DL-NIRSP gets Ca II which would prevent other coordinating instruments from using that wavelength (and longer wavelengths).  The on-limb continuum intensity is being used for the signal calculation, it is likely that the off-disk counterparts of the Ca II and He I lines have a lower intensity.  Using F/24 mode makes the observing cadence fast, sacrificing resolution, however the limb pointing may make AO correction difficult anyway.

\begin{figure}
	\begin{center}
		\includegraphics[width=5in]{./figures/input_window2.png}
		\includegraphics[width=1.67in]{./figures/output_window2a.png}\includegraphics[width=1.67in]{./figures/output_window2b.png}\includegraphics[width=1.67in]{./figures/output_window2c.png}
	\end{center}
	\caption{The calculator windows as they appear for a study of spicules on the limb.}
	\label{fig3}
\end{figure}


\subsection{AR Coronal Loops}
Here's an example of a big mosaic of the corona which nearly fills the field of view.  DL-NIRSP is making use of all channels, and probably not coordinating with other instruments.  It's likely a pointing like this would use the limb occulter.  Note that the scattered light parameter is set.  Starting exposure parameters were determined using the simple tab assuming a SNR of 100. 

\begin{figure}
	\begin{center}
		\includegraphics[width=5in]{./figures/input_window3.png}
		\includegraphics[width=1.67in]{./figures/output_window3a.png}\includegraphics[width=1.67in]{./figures/output_window3b.png}\includegraphics[width=1.67in]{./figures/output_window3c.png}
	\end{center}
	\caption{The calculator windows as they appear for a study of AR coronal loops.}
	\label{fig4}
\end{figure}

\clearpage

The coronal intensities are automatically estimated based on observations of the coronal lines, but can also be specified manually by selecting the ``Manual'' toggle.  This will open the small window shown in Figure \ref{fig4a} where the line parameters can be entered.

\begin{figure}
	\begin{center}
		\includegraphics[width=5in]{./figures/input_window3a.png}
			\end{center}
	\caption{Manual entry of coronal line parameters.}
	\label{fig4a}
\end{figure}

\begin{figure}
	\begin{center}
		\includegraphics[width=2.5in]{./figures/FeXIV_5303_IvsR.eps}\includegraphics[width=2.5in]{./figures/FeXI_7892_IvsR.eps}
		\includegraphics[width=2.5in]{./figures/FeXIII_10747_IvsR.eps}\includegraphics[width=2.5in]{./figures/FeXIII_10798_IvsR.eps}
		\includegraphics[width=2.5in]{./figures/HeI_10830_IvsR.eps}\includegraphics[width=2.5in]{./figures/SiX_14300_IvsR.eps}
	\end{center}
	\caption{Line and continuum intensity estimates for the DL-NIRSP spectral lines.  The line intensity is the peak spectral intensity of the line in units of disk center brightness at the corresponding wavelength.  We show the observations of \citet{kuhn96} and \citet{penn94}, and estimates from \citet{judge98} and one based on CHIANTI line synthesis of a quiet-Sun model provided by M. Hahn.}
	\label{fig5}
\end{figure}


\section{How it Works}
This outlines the steps for the calculation, see \verb+dlnirsp_ipc_calc+ and related subroutines for specific details.

\begin{enumerate}
	\item {\bf Intensity Estimate:}  Based on the pointing the calculator decides whether to use an on-disk or off-disk estimation of the intensities.  Scattered light may be considered for either situation.  The flux estimate returns an intensity in the units of [erg s$^{-1}$ sr$^{-1}$ cm$^{-2}$ \AA$^{-1}$] at the selected wavelengths.
	
	\begin{itemize}
		\item {\bf Disk Intensity Estimate:}  The calculator estimates the quiet-Sun intensity based on the $\mu$ value and wavelength using the normalized limb-darkening function and tabulated continuum intensity at disk center from \citet{aapq}.  The limb darkening functions are continuous in radius and discrete in wavelength, so the LDFs at a particular radius are multiplied by the disk center intensity and interpolated to the correct wavelength.
		
		\item {\bf Coronal Intensity Estimate:} The coronal continuum is estimated as a function of radial distance and wavelength based on the rough relations published in \cite{kimura98}.  The sky brightness (also from \cite{kimura98}) is assumed to be a fixed fraction of the solar intensity.  A scattered light fraction (based on DKIST estimates for the main mirror) is assumed to be a fixed fraction of the solar intensity.  The coronal line intensities as a function of radius are taken from \citet{penn94} for [Fe XIII] 1074.7 and [Si X] 1430.0 \AA, and from \citet{kuhn96} for He I 1083.0 \AA.  The coronal lines are assumed to have a width of 15 km/sec, this width is used with the total line intensity to calculate the line amplitude $I_0 = I_{tot} / (\sqrt{2\pi} \sigma)$.

		Alternately, the IPC takes the intensity, velocity shift, and width for the coronal lines using the manual entry option.  A comparison of different coronal continuum and coronal line intensities, including the ones used for the estimates in the IPC, is shown in Figure \ref{fig5} as a guideline for setting the line intensities.
		
		\item {\bf Scattered Light Estimate:}  Scattered light is added as a fixed fraction of the quiet-Sun disk center intensity at a particular wavelength, calculated for $\mu=1$.
	\end{itemize}
	
	\item {\bf Instrument Flux Calculation:}  The instrument flux calculation takes into account the telescope collecting area, plate scale and sampling for the resolution mode, the efficiencies for all optical surfaces, and the spectrograph efficiency.  It returns a photon flux in [photons $s^{-1}$ pixel$^{-1}$] at the DL-NIRSP detectors for the selected wavelength.

	\item{\bf Signal to Noise Calculation:} Based on the user-set exposure times and co-adds the number of detector counts (science signal + background signal + dark signal) is determined and compared to the camera full-well depth to determine if the exposure will be saturated.  The photon shot noise and other standard noise sources are combined.  The ratio of the science signal to noise is the signal to noise ratio.  In the on-disk case the science signal is the quiet-Sun continuum, and for the off-disk case it is the coronal emission line peak value.

	\item {\bf Duration and Data Volume Calculation:}  Once the exposure length and cadence is determined for each camera, the time per modulation sequence, tile, and mosaic are determined based on the modulator mode and scanning mode.  Discrete steps with the modulator and mosaic steps with the field scanning mirror require additional time for the mechanism to step and settle.  Based on the image size, the total data volume is determined, and dividing this by the time we get the data rate.
\end{enumerate}


\section{Functionality}
\subsection{Input}
\subsubsection{Image}
The calculator comes preloaded with an example set of images from SDO taken on July 7, 2014.  You can select the SDO channel that you want to see in the drop list.  Optionally, users can upload their own fits files. The fits file must have the WCS header parameters setting the coordinates and spatial scale (crval1, crpix1, cdelt1; crval2, crpix2, cdelt2).  Basic image scaling options are available, these are preset for the SDO image but you may need to adjust them for your input images.

\subsubsection{Target}
The pointing can be changed by clicking in the full-disk field of view in the left image or instrument field of view in the right image. The solar (x,y) coordinates will be updated. The coordinates can also be edited manually and should automatically update.

The field (for DL-NIRSP and all other instruments on the coud\'e rotator) may be rotated with respect to solar north to optimize field of view coverage or compensate for atmospheric diffraction.  A field rotation can be applied by entering it in the box.  This is just to demonstrate that rotation of the instrument is possible and the value set here is not actually used for the instrument operation or subsequent experiment generation steps.

\subsection{Intensities}
The user can choose how to set the coronal line intensities and scattered light fraction. Scattered light is an important part of the calculation of the coronal signal to noise estimate, and originates both from the observing system, and atmospheric conditions.  This is currently a very rough estimate, but it should only change the calculation significantly for off-disk pointings.

\subsubsection{Field of View}
DL-NIRSP has three resolution modes that can be selected in the drop down box:  Hi-Res mode with the f/62 feed optics and IFU-36 which critically samples the diffraction limit with 0.03" imaging pixels, Mid-Res mode with the f/24 feed optics and IFU-36 to provide 0.06" imaging pixels, and Wide Field mode with the f/24 feed and IFU-72 mounted with the coronal lens assembly to provide an f/8 beam and 0.5" imaging pixels for low light applications.  The user can set the number of times to repeat the mosaic, as well as the mosaic scan pattern.  There is currently only one scan pattern implemented (snake). The IFU field of view (green boxes) can be tiled to produce a mosaic as large as the 2.8' field of view (yellow circle).  The number of mosaic steps (NX,NY) can be set by entering an integer and the step size, in units of the IFU width and height, can be set.  There is also the option to conduct a dithered mosaic.  In the current implementation, the dither step takes place at one level higher than the mosaic step, i.e. one mosaic is conducted, then a second mosaic is conducted with the dither offset.

\subsection{FIDO Configuration}
There are two limiting cases for the FIDO configuration, one in which DL-NIRSP gets all wavelengths longer than 530 nm, and one in which DL-NIRSP get no visible light.

\subsubsection{Wavelength and Camera: Inverse Calculation}
DL-NIRSP has three spectrograph arms to observe up to three wavelengths simultaneously.  Select the wavelength for each of the spectrograph arms. (none) is a valid option.  For the inverse calculation tab, the user sets the desired signal to noise ratio and indicates which wavelength has priority for the calculation.  When this tab is active the calculator automatically determines the exposure time to avoid saturation with all the cameras, and determines the number of images to accumulate to meet the required signal to noise for the priority wavelength.  The exposure times for the non-priority channels are matched to the priority channel.

\subsubsection{Wavelength and Camera: Forward Calculation}
The forward calculation tab has the same wavelength options as the inverse calculation tab, but in this case the user must provide the number of non-destructive reads (NDR) and the number of co-adds.  When this tab is selected the calculation uses these parameters to determine the resulting signal to noise.  This tab adopts the settings determined by the inverse calculation. 

The DL-NIRSP infrared cameras operate in a ``fast up-the-ramp'' mode for on-disk observations with High-Res mode or ``sub-frame'' mode for on-disk observations with the Mid-Res or Wide Field modes.  Sub-frame mode is not yet implemented in the IPC.  For fast up-the-ramp mode, the camera has the option of resetting and reading charge in a non-destructive way once per frame.  A standard series of frames consists of a reset and read in close succession, this produces a frame with effectively zero integration time (a bias frame).  Then an arbitrary number of non-destructive reads may occur as the charge on the pixels increases.  An image with one NDR takes two frame times to produce, but it has an integration time equivalent to one frame time.  The functionality of the visible camera has been matched to the infrared camera for the purposes of the IPC.  Only particular exposure times are possible in this mode.  Observations on-disk with High-Res mode should use 1 or 2 NDR to avoid saturation of the IR cameras.  The number of accumulation cycles should be adjusted to provide better signal to noise if desired.
 
\subsubsection{Polarimeter}
DL-NIRSP has a single modulator which can be rotated continuously, in discrete steps, or not at all (no polarimetry).  The processing pipeline will only support 8-state modulation.  Continuous modulation is the standard option on disk.  Discrete stepping of the modulator incurs a movement penalty (system must wait for modulator to finish moving), but might be used for long exposures needed in the corona.
 
\subsection{Function Buttons}
\begin{description}
	\item{Calculate} will run the calculation and produce output text and plots.  Up until this button is pushed, nothing has been done except for FOV calculations.
	\item{Save} saves the state of the IPC to an IDL save file and produces a text file summary of inputs and outputs.
	\item{Reset} resets the input values and appearance of the GUI to how it appeared at startup.
	\item{Load} loads a saved state of the IPC from a previously made IDL save file.  You can also specify this file on startup with the restore keyword, either \verb+dlnirsp_ipc, restore=myfilename+ or \verb+dlnirsp_ipc, /restore+.
	\item{Quit} exits the IPC gracefully and resets IDL system variables to their original values.
\end{description}

\subsection{Output}
\begin{description}
	\item{Output} gives a summary of the results of the calculation including the signal estimates and data volumes.
	
	\item{Input} gives a summary of the inputs of the calculation.  Part of this is a rehash of the users input, but it also shows the static assumptions for the calculation and intermediate results of the calculation.
	
	\item{Spectrum Plot} shows a plot of the quiet-Sun spectrum, including scattered light and the coronal line intensity, coronal continuum intensity and sky brightness, in units of photons pixel$^{-1}$ sec$^{-1}$.
\end{description}


\subsection{Known Issues}
\textbf{You can make setups that use coronal diagnostics on-disk and photospheric diagnostics off-disk,} but there are off-disk intensity estimates only for [Fe XIII] 1074.7, He I 1083.0, and [Si X] 1430.0 nm.  We do not expect the photospheric lines of Fe I 630.2 nm and Fe I 1565.0 nm to have much of an off-disk component.

\textbf{The spectral arm selection is not actually independent.} 
The calculator uses the throughput and dispersion for each arm in an optimized configuration, but there is a single grating and finite length of travel for each arm. In reality each wavelength in each possible configuration may not be on blaze and will suffer from reduced throughput/slightly different dispersion.

\textbf{Changing the scanning modes does not do anything.} 
DL-NIRSP only has one scanning mode currently implemented, and that is the snake pattern.  If you need to know more about this pattern e.g. the direction of scanning, please contact me.

\textbf{The Mid-Res mode is at or above the IR detector saturation limit for on-disk cases using 1 NDR.}  
These cases require a different mode of the IR cameras (sub-frame mode) that has not yet been implemented into the IPC.

\textbf{Dither or Overlap Mosaic Steps?}
The DL-NIRSP mosaic has a dither mode available to help fill in gaps and artifacts due to the IFU.  When the Dither mode is selected, the instrument carries out two mosaics in series for each repeat of the mosaic.  The first is the normal mosaic, centered with respected to the instrument alignment.  The second mosaic has a specified offset with respect to the first mosaic.  For large mosaics, the dithered mosaic is separated in time by the time it takes to run one mosaic, which could be a long time during which the Sun has evolved.

The functionality of the dither mode may change in the future, but an alternative is possible.  The user can step the IFU by less than the integer multiple of the IFU size.  In this case, the overlapping steps in the mosaic will be nearer each other in time.  The longest separation in time between overlapping tiles will depend on the scanning mode, but it will be shorter than the time it takes to run the full mosaic.

\begin{figure}
	\centering
	\includegraphics[height=2in]{./figures/dither1.png}\includegraphics[width=3in]{./figures/dither1_settings.png}
	\caption{A $4\times4$ mosaic with a dither step equivalent to 0.25 the width and height of the integral field.}
	\label{fig:dither1}
\end{figure}

\begin{figure}
	\centering
	\includegraphics[height=2in]{./figures/dither2.png}\includegraphics[width=3in]{./figures/dither2_settings.png}
	\caption{A $5\times5$ mosaic with overlapping steps.}
	\label{fig:dither2}
\end{figure}

\textbf{Co-Add or Accumulate?}
In DKIST nomenclature, {\it co-adds} occur within each modulation state, and {\it accumulations} occur external to the modulation.
In continuous modes of the modulator, spurious polarization signal due to the seeing can be avoided by running the modulator very fast, so it is better to increase the number of accumulations external to the modulation cycle.  
In discrete modes of the modulator, there is additional time needed to move the modulator, so it is better to increase the number of co-adds inside the modulation cycle.
In single modes of the modulator, co-adds and accumulations have the same functionality.

For the moment, DL-NIRSP will always run at least two accumulations for continuous modulation modes.  This allows us to average over differences in the first and second half of the modulator rotation (one modulation cycle is only a $180^\circ$ rotation of the optic).

\textbf{Co-Add and Accumulations do not decrease data volume of IR cameras.}
If you increase the number of co-adds or accumulations for Arm 1, the data volume decreases because these frames are added together on the summit.  This is not currently the case for the IR cameras on Arms 2 and 3.  Frames from the IR cameras are not summed on the summit and every frame from a non-destructive read ramp is saved (number of non-destructive read frames plus the bias frame).  These frames will be processed at the data center.


\section{What's in the Folder}

\subsection{Summary of Routines}
\begin{description}
\item{\verb+dlnirsp_ipc+:} Main widget program, contains:

\verb+dlnirsp_ipc_setup+: Setup for the widget interface.

\verb+dlnirsp_ipc_event+: Event handler for the widget.

\verb+dlnirsp_ipc_set_state+: Sets the widget parameters to match the information in the internal data structures, i.e. after loading a previously saved output.

\verb+dlnirsp_ipc_get_state+: Gets the widget parameters from the widget and updates the internal data structures, i.e. prior to running a calculation or saving the state of the widget.

\verb+dlnirsp_ipc_check_fov+: Routine that checks if the mosaic is larger than the maximum field of view.

\verb+dlnirsp_ipc_update_disp+: Routine that refreshes the mosaic display.

\verb+dlnirsp_ipc_update_img+: Routine that updates the image shown in the display windows, if the selected image is changed. 

\verb+dlnirsp_ipc_update_spec+: Routine that updates the spectrum plots.

\verb+dlnirsp_ipc_oplot_circle+: Routine that overplots a circle on the plot.

\item{\verb+dlnirsp_ipc_input+} Generates the input and output structures, loads atlas spectra and throughput. Hard-coded parameters are set here.

\item{\verb+dlnirsp_ipc_getimg+} Routine which loads the image displayed and determines display ranges.

\item{\verb+dlnirsp_ipc_calc+:} The main level of the program that runs the calculation, contains:

\verb+dlnirsp_ipc_calc_phot+: Determines the photon rate per pixel at the detector for the given instrument and telescope parameters. 

\verb+dlnirsp_ipc_calc_snr+: Determines the signals and noises given the photon count rates and given the detector parameters. 

\verb+dlnirsp_ipc_calc_data+: Determines the amount of data generated by an observation and how long an observation takes. 

\verb+dlnirsp_ipc_calc_mosaic+: To be written, will calculate the mosaic positions and the order in which the mosaic steps.

\item{\verb+dlnirsp_ipc_disk+}: Estimates the limb-darkened quiet-Sun continuum intensity as a function of wavelength and radial position, based on tables taken from Allen's Astrophysical Quantities.

\item{\verb+dlnirsp_ipc_offdisk+:} Estimates the coronal line intensity, coronal continuum intensity, and sky background as a function of solar radius.

\item{\verb+dlnirsp_ipc_summsg+:} Generates the text for the input and output summaries.

\item{\verb+readfits+:} The fits reading program from the IDL Astronomy User's Library, see \url{https://idlastro.gsfc.nasa.gov/} for more information.  This routine relies on other routines also included with this distribution: \verb+cgerrormsg+, \verb+fxpar+, \verb+gettok+, \verb+sxaddpar+, \verb+sxpar+, and \verb+valid_num+.
\end{description}


\subsection{Summary of Data}
Additional data necessary for the calculation is contained in \verb+./data/+:

\begin{description}
\item{\verb+NSO_FTS_combined_atlas.sav+:} The combined visible and IR spectrum of the quiet-Sun from the FTS flux and photometric atlases.

\item{\verb+dlnirsp_throughput.sav+:} The calculated throughput of DKIST and DL-NIRSP for two limiting beam-splitter configurations (Vis+IR and IR only). Accounts for coatings and losses due to transmission but not areas.

\item{\verb+./data/SDO/+:} SDO fits images from 2014-07-07 00:00:00, which I think is a nice time.  Feel free to add your favorite SDO images (as fits files!) as user input.

\item{\verb+./data/coronal_intensities/+:} Coronal line intensities as a function of solar radius based on CHIANTI and coronal observations.
\end{description}


\section{Disclaimer}
We have done our best to estimate the signals, instrument performance, and noise sources for DL-NIRSP, but at this time the results of the IPC are still just an estimate.  There may be some discrepancy with the actual instrument performance.
%%
%\iffalse

\bibliographystyle{plainnat}
\begin{thebibliography}{5}

\bibitem[{Allen}(2000)]{aapq}
{Allen}, G.~W. {\it Allen's Astrophysical Quantities}

\bibitem[{Judge}(1998)]{judge98}
{Judge}, P.~G 1998, {\it ApJ}, 500, 1009

\bibitem[{Kimura \& Mann}(1998)]{kimura98}
{Kimura}, H. \& {Mann}, I. 1998, {\it Earth Planets Space}, 50, 493

\bibitem[{Kuhn, Penn, \& Mann}(1996)]{kuhn96}
{Kuhn}, J.~R., {Penn}, M.~J., \& {Mann}, I. 1996, {\it ApJL}, 456, 70

\bibitem[{Penn \& Kuhn}(1994)]{penn94}
{Penn}, M.~J. \& {Kuhn}, J.~R. 1994, {\it ApJ}, 434, 807 

\end{thebibliography}
%%
%\fi

\end{document}