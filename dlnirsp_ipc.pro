;+
; NAME:
;       dlnirsp_ipc
;
; PURPOSE:
;       A routine that interactivly sets parameters for the
;       Diffraction-Limited Infrared Spectropolarimeter and calculates
;       the resulting photon counts and data rates for the instrument.
;
;       The DL-NIRSP field of view is displayed on the user's
;       choice of SDO image.  The full range of SDO data is available
;       if SSW is invoked.
;
; CALLING SEQUENCE:
;       dlnirsp_ipc, [restore=restorefile]
;
; INPUTS:
;
; OPTIONAL INPUTS:
;       restore  - restore a previous configuration for tweaking.  If
;                  the restore keyword is just set, the user is
;                  prompted to pick a file, or a specific file name
;                  can be supplied.
;
; KEYWORD PARAMETERS:
;       None 
;
; OUTPUTS:
;       
; OPTIONAL OUTPUTS:
;       On request the code will save the configuration and results of
;       the calculator as an IDL save file with input and output data
;       structures and a text summary of the configuration.
;
; COMMON BLOCKS:
;       dlnirsp_environment
;       ipc_environment
;       mouse
;
; PROCEDURES USED:
;       dlnirsp_ipc_setup - a routine which sets all the default and
;                           hard-coded values, loads throughput table
;                           and spectral atlas
;       dlnirsp_ipc_calc  - where the actual calcualtion is done
;       dlnirsp_ondisk    - program that supplies the wavelength and
;                           pointing dependent estimates of
;                           photospheric solar intensities based on
;                           the disk center intensity and limb
;                           darkening in AApQ
;       dlnirsp_offdisk   - program that supplies the wavelength and
;                           pointing/structure dependent estimates of
;                           chromospheric and coronal inensitites off
;                           disk based on various published
;                           observations as documented in that program
;
;       Contained in this file:
;          dlnirsp_ipc_update_disp         
;          dlnirsp_ipc_update_img
;          dlnirsp_ipc_check_fov
;          dlnirsp_ipc_update_exp
;          dlnirsp_ipc_get_state
;          dlnirsp_ipc_set_state
;          dlnirsp_ipc_event
;          dlnirsp_ipc_setup
;
; COMMENTS:
;
; EXAMPLES:
;          IDL> dlnirsp_ipc
;
; MODIFICATION HISTORY:
;       Started 2016-Aug-25 by Sarah A. Jaeggli, National Solar
;               Observatory
;               2017-Oct-09 SAJ got super anal about naming
;               conventions.
;               2018-Jan-10 SAJ added search for routine in path to
;               identify the data directory. Updated camera
;               parameters. Changed format of fiber information
;               structure in input.  Took out factor for
;               non-destructive reads, I'm not sure the
;               assumption is right.
;               2018-03-22 SAJ changed some of the labels on the
;               widget.  Added further constraints on the inverse
;               calculation using line priority, further details in
;               dlnirsp_ipc_calc.
;               2019-09-30 SAJ decoupled the IFU from feed optics
;               options again. Radiometry estimates have been updated
;               to reflect the smaller core and larger length of IFU.
;               2022-05-24 SAJ made major changes to the IPC to give
;               the users control of the full mosaic functionality,
;               reflect the NDR feature of the cameras, and changes to
;               timing and parameter logic to reflect the real
;               operation of the instrument.
;               2024-03-12 SAJ made updates to exposure time logic for the
;               cameras.  Made another subroutine for this,
;               dlnirsp_ipc_update_exp.  Restructured the IPC so that things
;               fit better graphically.
;               2024-03-28 SAJ removed Inverse/Forward exposure time
;               calculation and added ability to use exposure time presets
;               based on the resolution and on/off disk target location.
;------------------------------------------------------------------------------

;;-----------------------------------------------------------------------------
PRO dlnirsp_ipc_oplot_circle, r, x0, y0, color=color, linestyle=linestyle
  t=findgen(361)
  
  xcoord = x0 + r*cos(t*!pi/180.)
  ycoord = y0 + r*sin(t*!pi/180.)
  
  oplot, xcoord, ycoord, color=color, linestyle=linestyle
END

PRO dlnirsp_ipc_update_disp

  COMMON ipc_environment, img, widx, didx, iidx, dir
  COMMON dlnirsp_environment, input, output, tp, atlas, lines
  
  WSET, didx.full
  if img.log then tmp=alog10(img.full.i) else tmp=img.full.i
  TV, bytscl(tmp, img.iran[0], img.iran[1])

  xran=([0.,img.orig.nx]-img.orig.x0)*img.orig.dx
  yran=([0.,img.orig.ny]-img.orig.y0)*img.orig.dy

  PLOT, xran, yran, /NODATA, XSTYLE=5, YSTYLE=5, /NOERASE

  dlnirsp_ipc_oplot_circle, input.feed[input.selected.feed].fov/2., $
                            input.target.x, input.target.y, COLOR='00ffff'x

  dlnirsp_ipc_oplot_circle, input.target.rsun, 0., 0., COLOR='FFFF00'x
  
  xbox=[-0.5,0.5,0.5,-0.5,-0.5]
  ybox=[-0.5,-0.5,0.5,0.5,-0.5]
  
  mos_xbox=input.ifu[input.selected.ifu].iput.sx * $
           input.feed[input.selected.feed].ps * xbox * $
           (1. + input.mosaic.dx * (input.mosaic.nx-1.))
  mos_ybox=input.ifu[input.selected.ifu].iput.sy * $
           input.feed[input.selected.feed].ps * ybox * $
           (1. + input.mosaic.dy * (input.mosaic.ny-1.))

  mos_xbox_rot=mos_xbox*cos(input.target.angle*!pi/180.) + $
               mos_ybox*sin(input.target.angle*!pi/180.) + $
               input.target.x

  mos_ybox_rot=mos_ybox*cos(input.target.angle*!pi/180.) - $
               mos_xbox*sin(input.target.angle*!pi/180.) + $
               input.target.y

  OPLOT, mos_xbox_rot, mos_ybox_rot, COLOR='00ff00'x 
  
  OPLOT, [input.target.x], [input.target.y], PSYM=1, COLOR='ff0000'x

  
  WSET, didx.zoom
  if img.log then tmp=alog10(img.zoom.i) else tmp=img.zoom.i
  TV, bytscl(tmp, img.iran[0], img.iran[1])

  
  xran=([0.,img.zoom.nx]-img.zoom.x0)*img.zoom.dx
  yran=([0.,img.zoom.ny]-img.zoom.y0)*img.zoom.dy

  PLOT, xran, yran, /NODATA, XSTYLE=5, YSTYLE=5, /NOERASE

  dlnirsp_ipc_oplot_circle, input.feed[input.selected.feed].fov/2., $
                            input.target.x, input.target.y, COLOR='00ffff'x, $
                            LINESTYLE=2
  dlnirsp_ipc_oplot_circle, input.target.rsun, 0., 0., COLOR='FFFF00'x

  ;;show the main mosaic
  for y=0,input.mosaic.ny-1 do begin
     for x=0,input.mosaic.nx-1 do begin
          mos_xbox=input.ifu[input.selected.ifu].iput.sx * $
                   input.feed[input.selected.feed].ps * $
                   (xbox + input.mosaic.dx*(x-input.mosaic.nx/2.+0.5))
          mos_ybox=input.ifu[input.selected.ifu].iput.sy * $
                   input.feed[input.selected.feed].ps * $
                   (ybox + input.mosaic.dy*(y-input.mosaic.ny/2.+0.5))

          mos_xbox_rot=mos_xbox*cos(input.target.angle*!pi/180.) + $
                       mos_ybox*sin(input.target.angle*!pi/180.) + $
                       input.target.x

          mos_ybox_rot=mos_ybox*cos(input.target.angle*!pi/180.) - $
                       mos_xbox*sin(input.target.angle*!pi/180.) + $
                       input.target.y

          OPLOT, mos_xbox_rot, mos_ybox_rot, COLOR='00ff00'x 
     endfor
  endfor

  ;;show the dithered mosaic
  if input.selected.dither then begin
     for y=0,input.mosaic.ny-1 do begin
        for x=0,input.mosaic.nx-1 do begin
           mos_xbox=input.ifu[input.selected.ifu].iput.sx * $
                    input.feed[input.selected.feed].ps * $
                    (xbox + input.mosaic.dx*(x-input.mosaic.nx/2.+0.5) + $
                     input.mosaic.ddx)
           mos_ybox=input.ifu[input.selected.ifu].iput.sy * $
                    input.feed[input.selected.feed].ps * $
                    (ybox + input.mosaic.dy*(y-input.mosaic.ny/2.+0.5) + $
                     input.mosaic.ddy)

           mos_xbox_rot=mos_xbox*cos(input.target.angle*!pi/180.) + $
                        mos_ybox*sin(input.target.angle*!pi/180.) + $
                        input.target.x

           mos_ybox_rot=mos_ybox*cos(input.target.angle*!pi/180.) - $
                        mos_xbox*sin(input.target.angle*!pi/180.) + $
                        input.target.y

           OPLOT, mos_xbox_rot, mos_ybox_rot, COLOR='90AA00'x 
        endfor
     endfor
  endif

  ;;show the center field position
  OPLOT, [input.target.x], [input.target.y], PSYM=1, COLOR='ff0000'x

END

;------------------------------------------------------------------------------

PRO dlnirsp_ipc_update_img, resample=resample
  
  COMMON ipc_environment, img, widx, didx, iidx, dir
  COMMON dlnirsp_environment, input, output, tp, atlas, lines
  
  if keyword_set(resample) ne 1 then begin
     data=DLNIRSP_IPC_GETIMG(img.channel, index, dir)

     rsun=index.rsun
     iran=index.iran
     log=index.log
     
     orig={i:data, nx:index.naxis1, ny:index.naxis2, $
           x0:index.crpix1, y0:index.crpix2, dx:index.cdelt1, dy:index.cdelt2}

     scl=FLOAT(img.nx)/FLOAT(orig.nx)
  
     full={i:CONGRID(data, img.nx, img.ny), nx:img.nx, ny:img.ny, $
           x0:orig.x0*scl, y0:orig.y0*scl, dx:orig.dx/scl, dy:orig.dy/scl}

     ;; reset scale toggle and bounds
     WIDGET_CONTROL, widx.imgscl[log], /SET_BUTTON
     WIDGET_CONTROL, widx.imgmin, SET_VALUE=iran[0]
     WIDGET_CONTROL, widx.imgmax, SET_VALUE=iran[1]
     
  endif else begin
     orig=img.orig
     full=img.full
     rsun=img.rsun
     iran=img.iran
     log=img.log
  endelse
  
  zoom=full
  
  marg=5.  ;display margins of zoom field in arcsec
     
  ;; field size in arcsec
  fieldx=input.ifu[input.selected.ifu].iput.sx * $
         input.feed[input.selected.feed].ps * $
         (1. + input.mosaic.dx*(input.mosaic.nx-1))
  fieldy=input.ifu[input.selected.ifu].iput.sy * $
         input.feed[input.selected.feed].ps * $
         (1. + input.mosaic.dy*(input.mosaic.ny-1))
   
  max_xy=SQRT(fieldx^2+fieldy^2) + 2.*marg
  
  zoom.dx=max_xy/zoom.nx & zoom.dy=max_xy/zoom.ny

  zoom.x0=(max_xy/2.-input.target.x)/zoom.dx
  zoom.y0=(max_xy/2.-input.target.y)/zoom.dy

  zoom_xpix=REBIN(findgen(zoom.nx, 1), zoom.nx, zoom.ny)
  zoom_ypix=REBIN(findgen(1, zoom.ny), zoom.nx, zoom.ny)

  zoom_xcoord=(zoom_xpix-zoom.x0)*zoom.dx
  zoom_ycoord=(zoom_ypix-zoom.y0)*zoom.dy
  
  orig_xcoord=zoom_xcoord/orig.dx+orig.x0
  orig_ycoord=zoom_ycoord/orig.dy+orig.y0
  
  zoom.i=INTERPOLATE(orig.i, orig_xcoord, orig_ycoord)
  
  img={orig:orig, full:full, zoom:zoom, channel:img.channel, $
       nx:img.nx, ny:img.ny, rsun:rsun, iran:iran, log:log}

END

;;-----------------------------------------------------------------------------

PRO dlnirsp_ipc_update_spec
  COMMON ipc_environment, img, widx, didx, iidx, dir
  COMMON dlnirsp_environment, input, output, tp, atlas, lines

  wset, didx.spec1
  if input.selected.wave1 eq 0 then nodata=1 else nodata=0
  PLOT, output.spectrum1.l, output.spectrum1.i, nodata=nodata, $
        xstyle=1, ystyle=1, yran=[0,max(output.spectrum1.i)*1.1], $
        xmargin=[7,2], ymargin=[3.5,1.75], $
        xtitle='Wavelength [nm]', ytitle='Intensity [I/I(Disk Center)]'
  
  wset, didx.spec2
  if input.selected.wave2 eq 0 then nodata=1 else nodata=0
  PLOT, output.spectrum2.l, output.spectrum2.i, nodata=nodata, $
        xstyle=1, ystyle=1, yran=[0,max(output.spectrum2.i)*1.1], $
        xmargin=[7,2], ymargin=[3.5,1.75], $
        xtitle='Wavelength [nm]', ytitle='Intensity [I/I(Disk Center)]'
  
  wset, didx.spec3
  if input.selected.wave3 eq 0 then nodata=1 else nodata=0
  PLOT, output.spectrum3.l, output.spectrum3.i, nodata=nodata, $
        xstyle=1, ystyle=1, yran=[0,max(output.spectrum3.i)*1.1], $
        xmargin=[7,2], ymargin=[3.5,1.75], $
        xtitle='Wavelength [nm]', ytitle='Intensity [I/I(Disk Center)]'
END

;;-----------------------------------------------------------------------------

PRO dlnirsp_ipc_check_fov, warning
  common dlnirsp_environment, input, output, tp, atlas, lines
  
  ;; make sure that mosaiced regions fall within the good field of view

  ;; center of AO field
  x0=input.target.x
  y0=input.target.y

  ;; radius of AO field
  rad=2.8*60./2.
  
  ;; total mosaic size
  mos_dx=input.ifu[input.selected.ifu].iput.sx * $
         input.feed[input.selected.feed].ps * $
         (1. + input.mosaic.dx*(input.mosaic.nx-1.))
  mos_dy=input.ifu[input.selected.ifu].iput.sy * $
         input.feed[input.selected.feed].ps * $
         (1. + input.mosaic.dy*(input.mosaic.ny-1.))

  ;; get positions of 4 corners
  xpos=[-mos_dx/2., mos_dx/2., $
        mos_dx/2., -mos_dx/2.]
  ypos=[-mos_dy/2., -mos_dy/2., $
        mos_dy/2., mos_dy/2.]

  xpos_rot=xpos*COS(input.target.angle*!pi/180.) + $
           ypos*SIN(input.target.angle*!pi/180.)
  ypos_rot=ypos*COS(input.target.angle*!pi/180.) - $
           xpos*SIN(input.target.angle*!pi/180.)
  
  ;; radius of the mosaic corner from the center of the AO field
  mos_rad=SQRT((xpos_rot)^2+(ypos_rot)^2)

  ;; check
  bad=WHERE(mos_rad gt rad, badcount)

  text='Some tiles of the '+STRING(input.mosaic.nx, format='(I3)')+' x'+STRING(input.mosaic.ny, format='(I3)')+' mosaic fall outside of the DKIST 2.8 arcmin field of view.  Please adjust the mosaic dimensions.'
  
  if badcount gt 0 then begin
     if KEYWORD_SET(warning) ne 1 then warning=text else warning=[warning,text]
  endif
  
END

;------------------------------------------------------------------------------

PRO dlnirsp_ipc_update_exp, ndr=ndr, exp=exp, coadd=coadd, $
                            cam1=cam1, cam2=cam2, cam3=cam3, $
                            update_all=update_all

  COMMON ipc_environment, img, widx, didx, iidx, dir
  COMMON dlnirsp_environment, input, output, tp, atlas, lines

  update_dur = 1
  
  if keyword_set(update_all) then begin
     update_ndr = 1
     update_exp = 1
     update_coadd = 1
  endif

  ;;Cam 1 exposure logic
  if keyword_set(cam1) then begin
     if keyword_set(ndr) then begin
        WIDGET_CONTROL, widx.cam1.ndr, GET_VALUE=val
        input.cam1.ndr = val
        
        ;;calculate equivalent exposure time
        input.cam1.exp = (input.cam1.ndr+1)/input.cam1.fr/2.

        update_exp = 1
     endif
        
     if keyword_set(exp) then begin
        WIDGET_CONTROL, widx.cam1.exp, GET_VALUE=val
        input.cam1.exp = val

        ;;calculate equivalent NDR
        input.cam1.ndr = ceil(input.cam1.exp*2.*input.cam1.fr) - 1

        update_ndr = 1
     endif

     if keyword_set(coadd) then begin
        WIDGET_CONTROL, widx.cam1.coadd, GET_VALUE=val
        input.cam1.coadd = val
     endif
     
     ;;calculate duration
     duration = (input.cam1.duty[0] + input.cam1.duty[1]*input.cam1.ndr) / $
                input.cam1.fr*input.cam1.coadd
     
     ;;update widget fields
     if keyword_set(update_ndr) then $
        WIDGET_CONTROL, widx.cam1.ndr, SET_VALUE=input.cam1.ndr
     if keyword_set(update_exp) then $
        WIDGET_CONTROL, widx.cam1.exp, SET_VALUE=input.cam1.exp
     if keyword_set(update_coadd) then $
        WIDGET_CONTROL, widx.cam1.coadd, SET_VALUE=input.cam1.coadd
     if keyword_set(update_dur) then $
        WIDGET_CONTROL, widx.cam1.dur, SET_VALUE=duration
  endif

  ;;Cam 2 exposure logic
  if keyword_set(cam2) then begin
     CASE input.selected.ircam OF
        0 : BEGIN               ;NDR Up-the-Ramp mode
           if keyword_set(ndr) then begin
              WIDGET_CONTROL, widx.cam2.ndr, GET_VALUE=val
              input.cam2.ndr = val

              ;;calculate equivalent exposure time
              input.cam2.exp = input.cam2.ndr/input.cam2.fr

              update_exp = 1
           endif

           if keyword_set(exp) then begin
              WIDGET_CONTROL, widx.cam2.exp, GET_VALUE=val
              input.cam2.exp = val

              ;;calculate equivalent NDR
              input.cam2.ndr = ceil(input.cam2.exp*input.cam2.fr)

              ;;make exposure time consistent
              input.cam2.exp = input.cam2.ndr/input.cam2.fr

              update_ndr = 1
           endif

           if keyword_set(update_ndr) then $
              WIDGET_CONTROL, widx.cam2.ndr, SET_VALUE=input.cam2.ndr
        END

        1 : BEGIN               ;SFR Sub-Frametime mode
           if keyword_set(ndr) then begin
              WIDGET_CONTROL, widx.cam2.ndr, GET_VALUE=val
              input.cam2.sfr = val

              ;;calculate equivalent exposure time
              input.cam2.exp = input.cam2.sfr/input.cam2.ny/input.cam2.fr

              update_exp = 1
           endif

           if keyword_set(exp) then begin
              WIDGET_CONTROL, widx.cam2.exp, GET_VALUE=val
              input.cam2.exp = val

              ;;calculate equivalent SFR
              input.cam2.sfr = ceil(input.cam2.exp*input.cam2.fr*input.cam2.ny)
              
              ;;make exposure time consistent
              input.cam2.exp = input.cam2.sfr/input.cam2.ny/input.cam2.fr

              update_ndr = 1
           endif

           if keyword_set(update_ndr) then $
              WIDGET_CONTROL, widx.cam2.ndr, SET_VALUE=input.cam2.sfr
        END
     ENDCASE

     if keyword_set(coadd) then begin
        WIDGET_CONTROL, widx.cam2.coadd, GET_VALUE=val
        input.cam2.coadd = val
     endif

     duration = (input.cam2.duty[0] + input.cam2.duty[1]*input.cam2.ndr) / $
                input.cam2.fr*input.cam2.coadd
     
     if keyword_set(update_exp) then $
        WIDGET_CONTROL, widx.cam2.exp, SET_VALUE=input.cam2.exp
     if keyword_set(update_coadd) then $
        WIDGET_CONTROL, widx.cam2.coadd, SET_VALUE=input.cam2.coadd
     if keyword_set(update_dur) then $
        WIDGET_CONTROL, widx.cam2.dur, SET_VALUE=duration
  endif

  ;;Cam 3 exposure logic, same as Cam 2
  if keyword_set(cam3) then begin
     CASE input.selected.ircam OF
        0 : BEGIN               ;NDR Up-the-Ramp mode
           if keyword_set(ndr) then begin
              WIDGET_CONTROL, widx.cam3.ndr, GET_VALUE=val
              input.cam3.ndr = val

              ;;calculate equivalent exposure time
              input.cam3.exp = input.cam3.ndr/input.cam3.fr

              update_exp = 1
           endif

           if keyword_set(exp) then begin
              WIDGET_CONTROL, widx.cam3.exp, GET_VALUE=val
              input.cam3.exp = val

              ;;calculate equivalent NDR
              input.cam3.ndr = ceil(input.cam3.exp*input.cam3.fr)

              ;;make exposure time consistent
              input.cam3.exp = input.cam3.ndr/input.cam3.fr

              update_ndr = 1
           endif
           
           if keyword_set(update_ndr) then $
              WIDGET_CONTROL, widx.cam3.ndr, SET_VALUE=input.cam3.ndr
        END

        1 : BEGIN               ;SFR Sub-Frametime mode
           if keyword_set(ndr) then begin
              WIDGET_CONTROL, widx.cam3.ndr, GET_VALUE=val
              input.cam3.sfr = val

              ;;calculate equivalent exposure time
              input.cam3.exp = input.cam3.sfr/input.cam3.ny/input.cam3.fr

              update_exp = 1
           endif

           if keyword_set(exp) then begin
              WIDGET_CONTROL, widx.cam3.exp, GET_VALUE=val
              input.cam3.exp = val

              ;;calculate equivalent SFR
              input.cam3.sfr = ceil(input.cam3.exp*input.cam3.fr*input.cam3.ny)
              
              ;;make exposure time consistent
              input.cam3.exp = input.cam3.sfr/input.cam3.ny/input.cam3.fr

              update_ndr = 1
           endif
     
           if keyword_set(update_ndr) then $
              WIDGET_CONTROL, widx.cam3.ndr, SET_VALUE=input.cam3.sfr
        END
     ENDCASE

     if keyword_set(coadd) then begin
        WIDGET_CONTROL, widx.cam3.coadd, GET_VALUE=val
        input.cam3.coadd = val
     endif

     duration = (input.cam3.duty[0] + input.cam3.duty[1]*input.cam3.ndr) / $
                input.cam3.fr*input.cam3.coadd
     
     if keyword_set(update_exp) then $
        WIDGET_CONTROL, widx.cam3.exp, SET_VALUE=input.cam3.exp
     if keyword_set(update_coadd) then $
        WIDGET_CONTROL, widx.cam3.coadd, SET_VALUE=input.cam3.coadd
     if keyword_set(update_dur) then $
        WIDGET_CONTROL, widx.cam3.dur, SET_VALUE=duration
  endif

END

;------------------------------------------------------------------------------

PRO dlnirsp_ipc_get_state

  COMMON ipc_environment, img, widx, didx, iidx, dir
  COMMON dlnirsp_environment, input, output, tp, atlas, lines

  WIDGET_CONTROL, widx.xcoord, GET_VALUE=val
  input.target.x=val
  WIDGET_CONTROL, widx.ycoord, GET_VALUE=val
  input.target.y=val
  WIDGET_CONTROL, widx.angle, GET_VALUE=val
  input.target.angle=val

  ;input.selected.corona = sqrt(input.target.x^2+input.target.y^2) gt $
  ;                        input.target.rsun
  
  idx=WIDGET_INFO(widx.scat, /DROPLIST_SELECT)
  input.selected.scat=idx

  idx=WIDGET_INFO(widx.feed, /DROPLIST_SELECT)
  case idx of
     0 : begin
        input.selected.feed=0
        input.selected.ifu=0
     end
           
     1 : begin
        input.selected.feed=1
        input.selected.ifu=0
     end
     
     2 : begin
        input.selected.feed=2
        input.selected.ifu=1
     end
           
     3 : begin
        input.selected.feed=0
        input.selected.ifu=2
     end
           
     4 : begin
        input.selected.feed=1
        input.selected.ifu=3
     end
  endcase
  
  WIDGET_CONTROL, widx.mosaicnl, GET_VALUE=val
  input.mosaic.nl=val
  idx=WIDGET_INFO(widx.mosaicpat, /DROPLIST_SELECT)
  input.selected.pat=idx
  
  WIDGET_CONTROL, widx.mosaicnx, GET_VALUE=val
  input.mosaic.nx=val
  WIDGET_CONTROL, widx.mosaicny, GET_VALUE=val
  input.mosaic.ny=val
  WIDGET_CONTROL, widx.mosaicdx, GET_VALUE=val
  input.mosaic.dx=val
  WIDGET_CONTROL, widx.mosaicdy, GET_VALUE=val
  input.mosaic.dy=val

  idx=WIDGET_INFO(widx.ditherTF, /BUTTON_SET)
  input.selected.dither=idx
  WIDGET_CONTROL, widx.ditherdx, GET_VALUE=val
  input.mosaic.ddx=val
  WIDGET_CONTROL, widx.ditherdy, GET_VALUE=val
  input.mosaic.ddy=val

  idx=WIDGET_INFO(widx.fido, /DROPLIST_SELECT)
  input.selected.fido=idx
  
  idx0 = WIDGET_INFO(widx.ircam[0], /BUTTON_SET)
  idx1 = WIDGET_INFO(widx.ircam[1], /BUTTON_SET)
  input.selected.ircam = where([idx0,idx1] eq 1)
  
  idx=WIDGET_INFO(widx.wave1, /DROPLIST_SELECT)
  input.selected.wave1=idx
;  WIDGET_CONTROL, widx.cam1.ndr, GET_VALUE=val
;  input.cam1.ndr=val
;  WIDGET_CONTROL, widx.cam1.exp, GET_VALUE=val
;  input.cam1.exp=val
;  WIDGET_CONTROL, widx.cam1.coadd, GET_VALUE=val
;  input.cam1.coadd=val
  
  idx=WIDGET_INFO(widx.wave2, /DROPLIST_SELECT)
  input.selected.wave2=idx
;  WIDGET_CONTROL, widx.cam2.ndr, GET_VALUE=val
;  input.cam2.ndr=val
;  WIDGET_CONTROL, widx.cam2.exp, GET_VALUE=val
;  input.cam2.exp=val
;  WIDGET_CONTROL, widx.cam2.coadd, GET_VALUE=val
;  input.cam2.coadd=val
  
  idx=WIDGET_INFO(widx.wave3, /DROPLIST_SELECT)
  input.selected.wave3=idx
;  WIDGET_CONTROL, widx.cam3.ndr, GET_VALUE=val
;  input.cam3.ndr=val
;  WIDGET_CONTROL, widx.cam3.exp, GET_VALUE=val
;  input.cam3.exp=val
;  WIDGET_CONTROL, widx.cam3.coadd, GET_VALUE=val
;  input.cam3.coadd=val
  
  WIDGET_CONTROL, widx.accum, GET_VALUE=val
  input.pol.accum=val

  ;;input.selected.pol
  idx0=WIDGET_INFO(widx.pol[0], /BUTTON_SET)
  idx2=WIDGET_INFO(widx.pol[1], /BUTTON_SET)
  idx3=WIDGET_INFO(widx.pol[2], /BUTTON_SET)
  input.selected.pol=where([idx0,idx2,idx3] eq 1)
  WIDGET_CONTROL, widx.nstates, GET_VALUE=val
  input.pol.nstates=val
END

;------------------------------------------------------------------------------

PRO dlnirsp_ipc_set_state
  
  COMMON ipc_environment, img, widx, didx, iidx, dir
  COMMON dlnirsp_environment, input, output, tp, atlas, lines
  
  WIDGET_CONTROL, widx.xcoord, SET_VALUE=input.target.x
  WIDGET_CONTROL, widx.ycoord, SET_VALUE=input.target.y
  WIDGET_CONTROL, widx.angle, SET_VALUE=input.target.angle

  WIDGET_CONTROL, widx.corona, SET_VALUE=input.selected.corona
  WIDGET_CONTROL, widx.scat, SET_DROPLIST_SELECT=input.selected.scat

  WIDGET_CONTROL, widx.feed, SET_DROPLIST_SELECT=input.selected.feed
  WIDGET_CONTROL, widx.mosaicnl, SET_VALUE=input.mosaic.nl
  WIDGET_CONTROL, widx.mosaicpat, SET_DROPLIST_SELECT=input.selected.pat
        
  WIDGET_CONTROL, widx.mosaicnx, SET_VALUE=input.mosaic.nx
  WIDGET_CONTROL, widx.mosaicny, SET_VALUE=input.mosaic.ny
  WIDGET_CONTROL, widx.mosaicdx, SET_VALUE=input.mosaic.dx
  WIDGET_CONTROL, widx.mosaicdy, SET_VALUE=input.mosaic.dy

  WIDGET_CONTROL, widx.ditherTF, SET_BUTTON=input.selected.dither
  WIDGET_CONTROL, widx.ditherdx, SET_VALUE=input.mosaic.ddx
  WIDGET_CONTROL, widx.ditherdy, SET_VALUE=input.mosaic.ddy
        
  WIDGET_CONTROL, widx.fido, SET_DROPLIST_SELECT=input.selected.fido
        
  WIDGET_CONTROL, widx.ircam[input.selected.ircam], /SET_BUTTON
  
  WIDGET_CONTROL, widx.wave1, SET_DROPLIST_SELECT=input.selected.wave1
  dlnirsp_ipc_update_exp, /update_all, /cam1
        
  WIDGET_CONTROL, widx.wave2, SET_DROPLIST_SELECT=input.selected.wave2
  dlnirsp_ipc_update_exp, /update_all, /cam2
        
  WIDGET_CONTROL, widx.wave3, SET_DROPLIST_SELECT=input.selected.wave3
  dlnirsp_ipc_update_exp, /update_all, /cam3
  
  WIDGET_CONTROL, widx.accum, SET_VALUE=input.pol.accum
        
  WIDGET_CONTROL, widx.pol[input.selected.pol], /SET_BUTTON
  WIDGET_CONTROL, widx.nstates, SET_VALUE=input.pol.nstates
END

;------------------------------------------------------------------------------

PRO dlnirsp_ipc_event, event

  COMMON ipc_environment, img, widx, didx, iidx, dir
  COMMON dlnirsp_environment, input, output, tp, atlas, lines
  COMMON mouse, mouseread
  
  stash=WIDGET_INFO(event.handler, /child)
  WIDGET_CONTROL, stash, GET_UVALUE=state

  WIDGET_CONTROL, event.id, GET_UVALUE=action

  CASE action OF

     'QUIT' : BEGIN
        WIDGET_CONTROL, widx.basein, /DESTROY
        
        ;; reset color and plot options to original
        !p.font=didx.font
        device, decomposed=didx.decomp
     END

     'CALC' : BEGIN
        ;; Reread all field values and update the input
        DLNIRSP_IPC_GET_STATE
        
        ;; Do the calculation
        DLNIRSP_IPC_CALC

        ;; Update camera exposure parameters
        DLNIRSP_IPC_SET_STATE

        ;; generate the text summaries
        DLNIRSP_IPC_SUMMSG, msgin, msgout
        
        ;; Return with the message/output
        WIDGET_CONTROL, widx.text_out, SET_VALUE=msgout
        WIDGET_CONTROL, widx.text_in, SET_VALUE=msgin
        
        ;; Show plots
        DLNIRSP_IPC_UPDATE_SPEC
        
        update=1 & resample=1
     END

     'LOAD' : BEGIN
        restorefile=DIALOG_PICKFILE(FILTER='*.sav')
        RESTORE, restorefile
        
        ;; set all field/menu values
        DLNIRSP_IPC_SET_STATE
        
        ;; Do the calculation
        DLNIRSP_IPC_CALC

        ;; generate the text summaries
        DLNIRSP_IPC_SUMMSG, msgin, msgout
        
        ;; Return with the message/output
        WIDGET_CONTROL, widx.text_out, SET_VALUE=msgout
        WIDGET_CONTROL, widx.text_in, SET_VALUE=msgin
        
        ;; Show plots
        DLNIRSP_IPC_UPDATE_SPEC
       
        update=1 & resample=1
     END
     
     'SAVE' : BEGIN
        ;; get all field/menu values
        DLNIRSP_IPC_GET_STATE
        
        ;; generate a save name
        CALDAT, SYSTIME(/JULIAN), mm, dd, yyyy, hh, nn, ss
        dt_string=STRING(yyyy,mm,dd,hh,nn,ss, $
                         FORMAT='(I04,I02,I02,"_",I02,I02,I02)')

        savname='DLNIRSP_IPC_'+dt_string+'.sav'
        
        ;; let user define path and savename
        savname=DIALOG_PICKFILE(path='', file=savname)

        tmp=strpos(savname, '.')
        if tmp[0] ne -1 then $
           txtname=strmid(savname, 0, tmp[-1])
        txtname+='.txt'
        
        if savname ne '' then begin
           SAVE, input, output, FILENAME=savname
        
           ;; get the contents of the text buffer
           WIDGET_CONTROL, widx.text_out, GET_VALUE=output_text
           WIDGET_CONTROL, widx.text_in, GET_VALUE=input_text

           text=[output_text, input_text]
        
           OPENW, lun, txtname, /GET_LUN
           for i=0,n_elements(text)-1 do $
              PRINTF, lun, text[i]
           FREE_LUN, lun
        
           ;; output message
           PRINT, ['Input and Output saved in '+savname, $
                   'Text Summary saved in '+txtname]
        endif else PRINT, 'File not saved'
        
     END

     'RESET' : BEGIN
        dlnirsp_ipc_input, dir
        
        ;; reset all values and widget appearance to original
        DLNIRSP_IPC_SET_STATE

        update=1 & resample=1

        ;; clear the message buffers
        WIDGET_CONTROL, widx.text_out, SET_VALUE=''
        WIDGET_CONTROL, widx.text_in, SET_VALUE=''
     END
     
     'IMG_CHANNEL' : BEGIN
        idx=WIDGET_INFO(widx.channel, /DROPLIST_SELECT)
        WIDGET_CONTROL, widx.channel, GET_VALUE=val
        img.channel=val[idx]
        update=1
     END

     'LINSCL' : BEGIN
        img.log=0
        update=1 & resample=1
     END
     
     'LOGSCL' : BEGIN
        img.log=1
        update=1 & resample=1
     END

     'IMG_IMIN' : BEGIN
        WIDGET_CONTROL, widx.imgmin, GET_VALUE=val
        img.iran[0]=val
        update=1 & resample=1
     END

     'IMG_IMAX' : BEGIN
        WIDGET_CONTROL, widx.imgmax, GET_VALUE=val
        img.iran[1]=val
        update=1 & resample=1
     END
        
     'XCOORD_ENTRY' : BEGIN
        WIDGET_CONTROL, widx.xcoord, GET_VALUE=val
        input.target.x=val
        update=1 & resample=1
     END
     
     'YCOORD_ENTRY' : BEGIN
        WIDGET_CONTROL, widx.ycoord, GET_VALUE=val
        input.target.y=val
        update=1 & resample=1
     END
     
     'XOFF_ENTRY' : BEGIN
        WIDGET_CONTROL, widx.xoff, GET_VALUE=val
        input.target.dx=val
        update=1 & resample=1
     END
     
     'YOFF_ENTRY' : BEGIN
        WIDGET_CONTROL, widx.yoff, GET_VALUE=val
        input.target.dy=val
        update=1 & resample=1
     END

     'ANGLE_ENTRY' : BEGIN
        WIDGET_CONTROL, widx.angle, GET_VALUE=val
        input.target.angle=val
        update=1 & resample=1
     END

     'SCAT_QUAL' : BEGIN
        idx=WIDGET_INFO(widx.scat, /DROPLIST_SELECT)
        input.selected.scat=idx
     END
     
     'FEED' : BEGIN
        idx=WIDGET_INFO(widx.feed, /DROPLIST_SELECT)
        
        ;; only need this if IFU is coupled to feed optics calculation
        case idx of
           0 : begin
              input.selected.feed=0
              input.selected.ifu=0
           end
           
           1 : begin
              input.selected.feed=1
              input.selected.ifu=0
           end
           
           2 : begin
              input.selected.feed=2
              input.selected.ifu=1
           end
        endcase
        
        update=1 & resample=1
     END

     'MOSAICNL_ENTRY' : BEGIN
        WIDGET_CONTROL, widx.mosaicnl, GET_VALUE=val
        input.mosaic.nl=val
     END

     'SCANPAT' : BEGIN
        idx=WIDGET_INFO(widx.mosaicpat, /DROPLIST_SELECT)
        input.selected.pat=idx
        print, 'The snake pattern is the only one currently implemented. Other patterns may be available in the future.'
     END
          
     'MOSAICNX_ENTRY' : BEGIN
        WIDGET_CONTROL, widx.mosaicnx, GET_VALUE=val
        input.mosaic.nx=val
        update=1 & resample=1
     END
     
     'MOSAICNY_ENTRY' : BEGIN
        WIDGET_CONTROL, widx.mosaicny, GET_VALUE=val
        input.mosaic.ny=val
        update=1 & resample=1
     END

     'MOSAICDX_ENTRY' : BEGIN
        WIDGET_CONTROL, widx.mosaicdx, GET_VALUE=val
        input.mosaic.dx=val
        update=1 & resample=1
     END

     'MOSAICDY_ENTRY' : BEGIN
        WIDGET_CONTROL, widx.mosaicdy, GET_VALUE=val
        input.mosaic.dy=val
        update=1 & resample=1
     END
     
     'DITHER' : BEGIN
        input.selected.dither=event.select
        ;;make dither text boxes (in)sensitive to input
        update=1 & resample=1
     END

     'DITHERDX_ENTRY' : BEGIN
        WIDGET_CONTROL, widx.ditherdx, GET_VALUE=val
        input.mosaic.ddx=val
        update=1 & resample=1
     END

     'DITHERDY_ENTRY' : BEGIN
        WIDGET_CONTROL, widx.ditherdy, GET_VALUE=val
        input.mosaic.ddy=val
        update=1 & resample=1
     END
     
     'FIDO' : BEGIN
        idx=WIDGET_INFO(widx.fido, /DROPLIST_SELECT)
        input.selected.fido=idx
     END
     
     'WAVE1' : BEGIN
        idx=WIDGET_INFO(widx.wave1, /DROPLIST_SELECT)
        input.selected.wave1=idx
     END
     
     'WAVE2' : BEGIN
        idx=WIDGET_INFO(widx.wave2, /DROPLIST_SELECT)
        input.selected.wave2=idx
     END
     
     'WAVE3' : BEGIN
        idx=WIDGET_INFO(widx.wave3, /DROPLIST_SELECT)
        input.selected.wave3=idx
     END

     'AUTOEXP' : BEGIN
        ;; fill in exposure parameters based on target and feed optics mode
        ;; using presets
        c = sqrt(input.target.x^2+input.target.y^2) gt input.target.rsun
        f = input.selected.feed

        input.selected.ircam = input.presets[f,c].ircam
        WIDGET_CONTROL, widx.ircam[input.selected.ircam], /SET_BUTTON
        
        WIDGET_CONTROL, widx.cam1.coadd, SET_VALUE=input.presets[f,c].cam1.coadd
        if input.presets[f,c].cam1.exp eq 0.0 then begin
           WIDGET_CONTROL, widx.cam1.ndr, SET_VALUE=input.presets[f,c].cam1.ndr
           dlnirsp_ipc_update_exp, /ndr, /coadd, /update_all, /cam1
        endif else begin
           WIDGET_CONTROL, widx.cam1.exp, SET_VALUE=input.presets[f,c].cam1.exp
           dlnirsp_ipc_update_exp, /exp, /coadd, /update_all, /cam1
        endelse
        
        WIDGET_CONTROL, widx.cam2.ndr, SET_VALUE=input.presets[f,c].cam2.ndr
        WIDGET_CONTROL, widx.cam2.coadd, SET_VALUE=input.presets[f,c].cam2.coadd
        dlnirsp_ipc_update_exp, /ndr, /coadd, /update_all, /cam2
        
        WIDGET_CONTROL, widx.cam3.ndr, SET_VALUE=input.presets[f,c].cam3.ndr
        WIDGET_CONTROL, widx.cam3.coadd, SET_VALUE=input.presets[f,c].cam3.coadd
        dlnirsp_ipc_update_exp, /ndr, /coadd, /update_all, /cam3
     END
     
     'NDR_PUSH' : BEGIN
        input.selected.ircam = 0

        input.cam1.ndr = 1
        input.cam1.coadd = 1
        
        input.cam2.sfr = 0
        input.cam2.ndr = 1
        input.cam2.coadd = 1

        input.cam3.sfr = 0
        input.cam3.ndr = 1
        input.cam3.coadd = 1

        WIDGET_CONTROL, widx.cam1.ndr, SET_VALUE=input.cam1.ndr
        WIDGET_CONTROL, widx.cam2.ndr, SET_VALUE=input.cam2.ndr
        WIDGET_CONTROL, widx.cam3.ndr, SET_VALUE=input.cam3.ndr
        
        dlnirsp_ipc_update_exp, /ndr, /update_all, /cam1
        dlnirsp_ipc_update_exp, /ndr, /update_all, /cam2
        dlnirsp_ipc_update_exp, /ndr, /update_all, /cam3
     END
     
     'SFR_PUSH' : BEGIN
        input.selected.ircam = 1

        input.cam1.ndr = 1
        input.cam1.coadd = 3
        
        input.cam2.sfr = 1023
        input.cam2.ndr = 1
        input.cam2.coadd = 3

        input.cam3.sfr = 1023
        input.cam3.ndr = 1
        input.cam3.coadd = 3

        WIDGET_CONTROL, widx.cam1.ndr, SET_VALUE=input.cam1.ndr
        WIDGET_CONTROL, widx.cam2.ndr, SET_VALUE=input.cam2.sfr
        WIDGET_CONTROL, widx.cam3.ndr, SET_VALUE=input.cam3.sfr
        
        dlnirsp_ipc_update_exp, /ndr, /update_all, /cam1
        dlnirsp_ipc_update_exp, /ndr, /update_all, /cam2
        dlnirsp_ipc_update_exp, /ndr, /update_all, /cam3
     END
     
     'NDR1_ENTRY' : BEGIN
        dlnirsp_ipc_update_exp, /ndr, /cam1
     END

     'EXP1_ENTRY' : BEGIN
        dlnirsp_ipc_update_exp, /exp, /cam1
     END

     'CAD1_ENTRY' : BEGIN
        dlnirsp_ipc_update_exp, /coadd, /cam1
     END

     'NDR2_ENTRY' : BEGIN
        dlnirsp_ipc_update_exp, /ndr, /cam2
     END

     'EXP2_ENTRY' : BEGIN
        dlnirsp_ipc_update_exp, /exp, /cam2
     END

     'CAD2_ENTRY' : BEGIN
        dlnirsp_ipc_update_exp, /coadd, /cam2
     END

     'NDR3_ENTRY' : BEGIN
        dlnirsp_ipc_update_exp, /ndr, /cam3
     END

     'EXP3_ENTRY' : BEGIN
        dlnirsp_ipc_update_exp, /exp, /cam3
     END

     'CAD3_ENTRY' : BEGIN
        dlnirsp_ipc_update_exp, /coadd, /cam3
     END

     'ACCUM' : BEGIN
        WIDGET_CONTROL, widx.accum, GET_VALUE=val
        accum=ceil(val/2.)*2
        if accum ne val and input.selected.pol ge 1 then $
           print, 'Number of accumulation cycles must be a factor of two for polarimetric modes, for the moment.'
        input.pol.accum=accum
     END
     
     'POLOFF' : BEGIN
        input.selected.pol=0
        input.pol.nstates=1
        WIDGET_CONTROL, widx.nstates, $
                        SET_VALUE=string(input.pol.nstates, format='(I2)')
     END
     
     'POLCONT' : BEGIN
        input.selected.pol=1
        input.pol.nstates=8
        WIDGET_CONTROL, widx.nstates, $
                        SET_VALUE=string(input.pol.nstates, format='(I2)')
     END
     
     'POLDISC' : BEGIN
        input.selected.pol=2
        input.pol.nstates=8
        WIDGET_CONTROL, widx.nstates, $
                        SET_VALUE=string(input.pol.nstates, format='(I2)')
     END
     
     'NSTATES' : BEGIN
        WIDGET_CONTROL, widx.nstates, GET_VALUE=val
        input.pol.nstates=val
     END
     
     'FULL_DISPLAY' : BEGIN
        if event.release eq 1 then begin
           ;; turn pixel coordinates into solar coordinates
           input.target.x=(event.x-img.full.x0)*img.full.dx
           input.target.y=(event.y-img.full.y0)*img.full.dy
           
           update=1 & resample=1
           
           WIDGET_CONTROL, widx.xcoord, SET_VALUE=input.target.x
           WIDGET_CONTROL, widx.ycoord, SET_VALUE=input.target.y
        endif
     END

     'ZOOM_DISPLAY' : BEGIN
        if event.release eq 1 then begin
           ;; turn pixel coordinates into solar coordinates
           input.target.x=(event.x-img.zoom.x0)*img.zoom.dx
           input.target.y=(event.y-img.zoom.y0)*img.zoom.dy
           
           update=1 & resample=1
           
           WIDGET_CONTROL, widx.xcoord, SET_VALUE=input.target.x
           WIDGET_CONTROL, widx.ycoord, SET_VALUE=input.target.y
        endif
     END

     'WAVECAM_TAB_SEL' : BEGIN
        ;; check if the beginning or advanced tab is selected
        idx=WIDGET_INFO(widx.input_tabs, /TAB_CURRENT)
        input.selected.advanced=idx
     END

     'OUT_TAB_SEL' : BEGIN
     END

     
     ;; Coronal Intensity Settings and mini-widget
     ;;---------------------------------------
     'INT_CORONA' : BEGIN
        input.selected.corona=event.value
        
        if event.value eq 1 then begin
           
           ;; if sub-widget is not already open...
           if XREGISTERED('dlnirsp_ipc_widget_int') eq 0 then begin
              DLNIRSP_IPC_INTEN_SETUP
              WIDGET_CONTROL, iidx.base, /REALIZE

              XMANAGER, 'dlnirsp_ipc_widget_int', iidx.base, $
                        EVENT_HANDLER='dlnirsp_ipc_event', /NO_BLOCK, $
                        GROUP_LEADER=widx.basein
           endif else print, 'Coronal intensity widget is already open.'
           
        endif else begin
           
           if XREGISTERED('dlnirsp_ipc_widget_int') eq 1 then begin
              WIDGET_CONTROL, iidx.base, /DESTROY
           endif
           
        endelse
     END

     'LINE1I' : BEGIN
        WIDGET_CONTROL, iidx.line[0].i, GET_VALUE=val
        input.inten.line[0].i=val
     END
     'LINE1V' : BEGIN
        WIDGET_CONTROL, iidx.line[0].v, GET_VALUE=val
        input.inten.line[0].v=val
     END
     'LINE1W' : BEGIN
        WIDGET_CONTROL, iidx.line[0].dw, GET_VALUE=val
        input.inten.line[0].dw=val
     END

     'LINE2I' : BEGIN
        WIDGET_CONTROL, iidx.line[1].i, GET_VALUE=val
        input.inten.line[1].i=val
     END
     'LINE2V' : BEGIN
        WIDGET_CONTROL, iidx.line[1].v, GET_VALUE=val
        input.inten.line[1].v=val
     END
     'LINE2W' : BEGIN
        WIDGET_CONTROL, iidx.line[1].dw, GET_VALUE=val
        input.inten.line[1].dw=val
     END

     'LINE3I' : BEGIN
        WIDGET_CONTROL, iidx.line[2].i, GET_VALUE=val
        input.inten.line[2].i=val
     END
     'LINE3V' : BEGIN
        WIDGET_CONTROL, iidx.line[2].v, GET_VALUE=val
        input.inten.line[2].v=val
     END
     'LINE3W' : BEGIN
        WIDGET_CONTROL, iidx.line[2].dw, GET_VALUE=val
        input.inten.line[2].dw=val
     END
     
     'DISMISS' : BEGIN
        WIDGET_CONTROL, iidx.base, /DESTROY
     END
     ;;---------------------------------------

  ENDCASE

  if keyword_set(update) then begin
     DLNIRSP_IPC_UPDATE_IMG, RESAMPLE=resample
     DLNIRSP_IPC_UPDATE_DISP
     DLNIRSP_IPC_CHECK_FOV
  endif

END

;;-----------------------------------------------------------------------------

PRO dlnirsp_ipc_inten_setup

  COMMON ipc_environment, img, widx, didx, iidx, dir
  COMMON dlnirsp_environment, input, output, tp, atlas, lines
  
  base = WIDGET_BASE(TITLE='Manual Coronal Line Parameters', /COLUMN)
  label = WIDGET_LABEL(base, VALUE='          Peak Intensity [I/I(sun)]         Velocity [km/s]             Width [km/s]', /ALIGN_LEFT)
  
  ;; Arm 1 line
  base1 = WIDGET_BASE(base, /ROW)
  int1 = CW_FIELD(base1, VALUE=input.inten.line[0].i, /ALL_EVENTS, $
                  UVALUE='LINE1I', TITLE='Arm 1 Line')
  vel1 = CW_FIELD(base1, VALUE=input.inten.line[0].v, /ALL_EVENTS, $
                  UVALUE='LINE1V', TITLE='')
  wid1 = CW_FIELD(base1, VALUE=input.inten.line[0].dw, /ALL_EVENTS, $
                  UVALUE='LINE1W', TITLE='')
      
  ;; Arm 2 line
  base2 = WIDGET_BASE(base, /ROW)
  int2 = CW_FIELD(base2, VALUE=input.inten.line[1].i, /ALL_EVENTS, $
                  UVALUE='LINE2I', TITLE='Arm 2 Line')
  vel2 = CW_FIELD(base2, VALUE=input.inten.line[1].v, /ALL_EVENTS, $
                  UVALUE='LINE2V', TITLE='')
  wid2 = CW_FIELD(base2, VALUE=input.inten.line[1].dw, /ALL_EVENTS, $
                  UVALUE='LINE2W', TITLE='')
  
  ;; Arm 3 line
  base3 = WIDGET_BASE(base, /ROW)
  int3 = CW_FIELD(base3, VALUE=input.inten.line[2].i, /ALL_EVENTS, $
                  UVALUE='LINE3I', TITLE='Arm 3 Line')
  vel3 = CW_FIELD(base3, VALUE=input.inten.line[2].v, /ALL_EVENTS, $
                  UVALUE='LINE3V', TITLE='')
  wid3 = CW_FIELD(base3, VALUE=input.inten.line[2].dw, /ALL_EVENTS, $
                  UVALUE='LINE3W', TITLE='')

  button = WIDGET_BUTTON(base, VALUE='Dismiss', UVALUE='DISMISS')
  
  iidx={base:base, $
        line:[{i:int1, v:vel1, dw:wid1}, $
              {i:int2, v:vel2, dw:wid2}, $
              {i:int3, v:vel3, dw:wid3}] }
  
END

;;------------------------------------------------------------------------------

PRO dlnirsp_ipc_setup

  COMMON ipc_environment, img, widx, didx, iidx, dir
  COMMON dlnirsp_environment, input, output, tp, atlas, lines
  
  inblock=WIDGET_BASE(TITLE='DL-NIRSP IPC Input', /ROW)
  inblock_col1=WIDGET_BASE(inblock, /COLUMN)
  inblock_col2=WIDGET_BASE(inblock, /COLUMN)
  
  outblock=WIDGET_BASE(TITLE='DL-NIRSP IPC Output', /COLUMN, $
                       GROUP_LEADER=inblock, xoffset=1028, yoffset=0)
  
  ;; image selection block
  selblock = WIDGET_BASE(inblock_col1, /ROW)

  ;; Image selection dropdown box, default SDO image set
  channel_list = WIDGET_DROPLIST(selblock, UVALUE='IMG_CHANNEL', TAB_MODE=1, $
                                 TITLE='Image', $
                                 VALUE=['HMI Magnetogram', $
                                        'HMI Intensitygram',$
                                        'AIA 1700', $
                                        'AIA 304', $
                                        'AIA 193', $
                                        'AIA 171', $
                                        'User Input'])

  sclblock = WIDGET_BASE(selblock, /ROW, /EXCLUSIVE)
  scl_lin = WIDGET_BUTTON(sclblock, VALUE='Linear', UVALUE='LINSCL')
  scl_log = WIDGET_BUTTON(sclblock, VALUE='Log', UVALUE='LOGSCL')
  
  imin_text=CW_FIELD(selblock, VALUE=0., UVALUE='IMG_IMIN', $
                     TITLE='I min,max:', XSIZE=5, /FLOAT, $
                     /ALL_EVENTS)
  
  imax_text=CW_FIELD(selblock, VALUE=100., UVALUE='IMG_IMAX', $
                     TITLE=',', XSIZE=5, /FLOAT, $
                     /ALL_EVENTS)

  
  ;; Image display block
  imgblock = WIDGET_BASE(inblock_col1, /ROW)
  
  full_draw = WIDGET_DRAW(imgblock, UVALUE='FULL_DISPLAY', $
                         XSIZE=img.nx, YSIZE=img.ny, $
                         /BUTTON_EVENTS, RETAIN=2, FRAME=1)
  
  zoom_draw = WIDGET_DRAW(imgblock, uvalue='ZOOM_DISPLAY', $
                          XSIZE=img.nx, YSIZE=img.ny, $
                          /BUTTON_EVENTS, RETAIN=2, FRAME=1)
  
  
  ;; Target parameter block
  block1= WIDGET_BASE(inblock_col1, /COLUMN, FRAME=2)
  label = WIDGET_LABEL(block1, VALUE='Target', /ALIGN_LEFT)
  
  targetblock1=WIDGET_BASE(block1, /ROW)
  coordx_text = CW_FIELD(targetblock1, VALUE=input.target.x, /ALL_EVENTS, $
                         UVALUE='XCOORD_ENTRY', TITLE='Solar X,Y ["]', XSIZE=5)
  coordy_text = CW_FIELD(targetblock1, VALUE=input.target.y, /ALL_EVENTS, $
                         UVALUE='YCOORD_ENTRY', TITLE=',', XSIZE=5)
  angle_text = CW_FIELD(targetblock1, VALUE=input.target.angle, /ALL_EVENTS, $
                        UVALUE='ANGLE_ENTRY', TITLE='Rotation [deg]', xsize=5)

  
  ;; Intensity input options
  intensityblock=WIDGET_BASE(inblock_col1, /COLUMN, FRAME=2)
  label = WIDGET_LABEL(intensityblock, VALUE='Intensities', /ALIGN_LEFT)

  intensityblock2 = WIDGET_BASE(intensityblock, /ROW)
  text = WIDGET_LABEL(intensityblock2, VALUE='Coronal Line Intensities')

  corona_bgroup = CW_BGROUP(intensityblock2, ['Automatic','Manual'], $
                            SET_VALUE=input.selected.corona, /NO_RELEASE, $
                            /EXCLUSIVE, /ROW, UVALUE='INT_CORONA')
  
  intensityblock1=WIDGET_BASE(intensityblock, /ROW)
  qual_list = WIDGET_DROPLIST(intensityblock1, VALUE=input.inten.scat.qual + $
                              string(input.inten.scat.val, FORMAT='(x,E7.1)'), $
                              UVALUE='SCAT_QUAL', $
                              TITLE='Scattered Light Conditions [I/I(Sun)]')

  ;; FIDO configuration
  ;; beamsplitter configuration
  fido_list = WIDGET_DROPLIST(intensityblock, VALUE=input.fido.name, $
                              /ALIGN_LEFT, UVALUE='FIDO', $
                              TITLE='FIDO Beam Splitter Configuration')

  ;; Mosaic and FOV parameter block
  mosaicblock = WIDGET_BASE(inblock_col2, /COLUMN, FRAME=2)
  ;label = WIDGET_LABEL(mosaicblock, VALUE='Moasic', /ALIGN_LEFT)
  feed_list = WIDGET_DROPLIST(mosaicblock, VALUE=input.feed.name, $
                              UVALUE='FEED', TITLE='Resolution Mode')
  
  mosaicblock1 = WIDGET_BASE(mosaicblock, /ROW)
  mosaicnl_text = CW_FIELD(mosaicblock1, VALUE=input.mosaic.nl, $
                           /ALL_EVENTS, /INTEGER, $
                           UVALUE='MOSAICNL_ENTRY', $
                           TITLE='Mosaic # of Repeats', XSIZE=5)
  scanpat_list = WIDGET_DROPLIST(mosaicblock1, VALUE=input.mosaic.pat, $
                                 UVALUE='SCANPAT', TITLE='Scan mode')
  
  mosaicblock2 = WIDGET_BASE(mosaicblock, /ROW, /ALIGN_RIGHT)
  label = WIDGET_LABEL(mosaicblock2, VALUE='Mosaic')
  mosaicnx_text = CW_FIELD(mosaicblock2, VALUE=input.mosaic.nx, $
                           /ALL_EVENTS, /INTEGER, $
                           UVALUE='MOSAICNX_ENTRY', /COLUMN, $
                           TITLE='NX',XSIZE=5)
  mosaicny_text = CW_FIELD(mosaicblock2, VALUE=input.mosaic.ny, $
                           /ALL_EVENTS, /INTEGER, $
                           UVALUE='MOSAICNY_ENTRY', /COLUMN, $
                           TITLE='NY', XSIZE=5)
  mosaicdx_text = CW_FIELD(mosaicblock2, VALUE=input.mosaic.dx, /ALL_EVENTS, $
                           UVALUE='MOSAICDX_ENTRY', /COLUMN, XSIZE=5, $
                           TITLE='dX')
  mosaicdy_text = CW_FIELD(mosaicblock2, VALUE=input.mosaic.dy, /ALL_EVENTS, $
                           UVALUE='MOSAICDY_ENTRY', /COLUMN, $
                           TITLE='dY', XSIZE=5)
  label = WIDGET_LABEL(mosaicblock2, VALUE='[Fraction IFU]')
  
  mosaicblock3 = WIDGET_BASE(mosaicblock, /ROW, /ALIGN_RIGHT)
  dither_base = WIDGET_BASE(mosaicblock3, /NONEXCLUSIVE, /ALIGN_BOTTOM)
  dither_button = WIDGET_BUTTON(dither_base, /PUSHBUTTON_EVENTS, $
                                VALUE='Dither', UVALUE='DITHER')
  ditherdx_text = CW_FIELD(mosaicblock3, VALUE=input.mosaic.ddx, /ALL_EVENTS, $
                           UVALUE='DITHERDX_ENTRY', /COLUMN, $
                           TITLE='dX', XSIZE=5, /NOEDIT)
  ditherdy_text = CW_FIELD(mosaicblock3, VALUE=input.mosaic.ddy, /ALL_EVENTS, $
                           UVALUE='DITHERDY_ENTRY', /COLUMN, $
                           TITLE='dY', XSIZE=5, /NOEDIT)
  label = WIDGET_LABEL(mosaicblock3, VALUE='[Fraction IFU]')

  
  ;; Spectrograph arm parameter block
  specblock = WIDGET_BASE(inblock_col2, /COLUMN, FRAME=2)
  label = WIDGET_LABEL(specblock, VALUE='Spectrograph Configuration', /ALIGN_LEFT)
  
  ;; spectrograph configuration
  specblock1 = WIDGET_BASE(specblock, /ROW, XPAD=0)
  
  waveblock1 = WIDGET_BASE(specblock1, /COLUMN, XPAD=0)
  label = WIDGET_LABEL(waveblock1, VALUE='Arm 1')
  wave_list1 = WIDGET_DROPLIST(waveblock1, UVALUE='WAVE1', TITLE='', $
                               VALUE=input.wave1.name)
  
  waveblock2 = WIDGET_BASE(specblock1, /COLUMN, XPAD=0)
  label = WIDGET_LABEL(waveblock2, VALUE='Arm 2')
  wave_list2 = WIDGET_DROPLIST(waveblock2, UVALUE='WAVE2', TITLE='', $
                               VALUE=input.wave2.name)
  
  waveblock3 = WIDGET_BASE(specblock1, /COLUMN, XPAD=0)
  label = WIDGET_LABEL(waveblock3, VALUE='Arm 3')
  wave_list3 = WIDGET_DROPLIST(waveblock3, UVALUE='WAVE3', TITLE='', $
                               VALUE=input.wave3.name)
  

  ;; Exposure Times
  expblock = WIDGET_BASE(inblock_col2, /COLUMN, FRAME=2)

  expblock0c = WIDGET_BASE(expblock, /ROW)
  label = WIDGET_LABEL(expblock0c, VALUE='Camera Configuration                             ', $
                       /ALIGN_LEFT)
  button = WIDGET_BUTTON(expblock0c, VALUE='Automatic Exposure Setup', $
                         UVALUE='AUTOEXP', /ALIGN_RIGHT)
  
  ;; Forward Calculation Settings
  ;; NDR vs SFR Option
  expblock0a = WIDGET_BASE(expblock, /ROW)
  label = WIDGET_LABEL(expblock0a, VALUE='*IR Camera Mode')
  ircam_base = WIDGET_BASE(expblock0a, /EXCLUSIVE, /ROW)
  ndr_button = WIDGET_BUTTON(ircam_base, /PUSHBUTTON_EVENTS, $
                             VALUE='NDR Up-the-Ramp', UVALUE='NDR_PUSH') 
  sfr_button = WIDGET_BUTTON(ircam_base, /PUSHBUTTON_EVENTS, $
                             VALUE='SFR Sub-Frametime', UVALUE='SFR_PUSH')
  WIDGET_CONTROL, NDR_button, SET_BUTTON=input.selected.ircam
  
  ;; labels
  expblock0b = WIDGET_BASE(expblock, /ROW)
  label = WIDGET_LABEL(expblock0b, VALUE='        NDR/SFR   Exp [s]    Co-Add    Duration [s]', $
                       /ALIGN_LEFT)
  
  ;; Arm 1
  expblock1 = WIDGET_BASE(expblock, /ROW, XPAD=0)
  label = WIDGET_LABEL(expblock1, VALUE='Arm 1 ')
  ndr_text1 = CW_FIELD(expblock1, TITLE='', UVALUE='NDR1_ENTRY', /ALL_EVENTS, $
                       VALUE=input.cam1.ndr, /INTEGER, XSIZE=6)
  exp_text1 = CW_FIELD(expblock1, TITLE='', UVALUE='EXP1_ENTRY', /ALL_EVENTS, $
                       VALUE=input.cam1.exp, XSIZE=6)
  cad_text1 = CW_FIELD(expblock1, TITLE='', UVALUE='CAD1_ENTRY', /ALL_EVENTS, $
                       VALUE=input.cam1.coadd, /INTEGER, XSIZE=6)
  dur_text1 = CW_FIELD(expblock1, TITLE='', /NOEDIT, XSIZE=6)

  ;; Arm 2
  expblock2 = WIDGET_BASE(expblock, /ROW, XPAD=0)
  label = WIDGET_LABEL(expblock2, VALUE='Arm 2*')
  ndr_text2 = CW_FIELD(expblock2, TITLE='', UVALUE='NDR2_ENTRY', /ALL_EVENTS, $
                       VALUE=input.cam2.ndr, /INTEGER, XSIZE=6)
  exp_text2 = CW_FIELD(expblock2, TITLE='', UVALUE='EXP2_ENTRY', /ALL_EVENTS, $
                       VALUE=input.cam2.exp, XSIZE=6)
  cad_text2 = CW_FIELD(expblock2, TITLE='', UVALUE='CAD2_ENTRY', /ALL_EVENTS, $
                       VALUE=input.cam2.coadd, /INTEGER, XSIZE=6)
  dur_text2 = CW_FIELD(expblock2, TITLE='', /NOEDIT, XSIZE=6)
  
  ;; Arm 3
  expblock3 = WIDGET_BASE(expblock, /ROW, XPAD=0)
  label = WIDGET_LABEL(expblock3, VALUE='Arm 3*')
  ndr_text3 = CW_FIELD(expblock3, TITLE='', UVALUE='NDR3_ENTRY', /ALL_EVENTS, $
                       VALUE=input.cam3.ndr, /INTEGER, XSIZE=6)
  exp_text3 = CW_FIELD(expblock3, TITLE='', UVALUE='EXP3_ENTRY', /ALL_EVENTS, $
                       VALUE=input.cam3.exp, xsize=6)
  cad_text3 = CW_FIELD(expblock3, TITLE='', UVALUE='CAD3_ENTRY', /ALL_EVENTS, $
                       VALUE=input.cam3.coadd, /INTEGER, XSIZE=6)
  dur_text3 = CW_FIELD(expblock3, TITLE='', /NOEDIT, XSIZE=6)
  
  
  ;; polarimetry block
  block4 = WIDGET_BASE(inblock_col2, /COLUMN, FRAME=2)

  block4a = WIDGET_BASE(block4, /ROW)
  label = WIDGET_LABEL(block4a, VALUE='Polarimeter')
  
  block4b = WIDGET_BASE(block4a, /ROW, /EXCLUSIVE)
  pol1 = WIDGET_BUTTON(block4b, VALUE='Off (Stokes I only)', UVALUE='POLOFF')
  pol2 = WIDGET_BUTTON(block4b, VALUE='Continuous', UVALUE='POLCONT')
  pol3 = WIDGET_BUTTON(block4b, VALUE='Discrete', UVALUE='POLDISC')
  WIDGET_CONTROL, pol2, SET_BUTTON=input.selected.pol

  ;; Modulation Steps
  block4c = WIDGET_BASE(block4, /ROW)
  pol_text = CW_FIELD(block4c, TITLE='N States', VALUE=input.pol.nstates, $
                      xsize=4, /NOEDIT)
  
  ;; Accumulations
  accum_text = CW_FIELD(block4c, TITLE='Accumulations', UVALUE='ACCUM', $
                        /ALL_EVENTS, VALUE=input.pol.accum, /INTEGER, $
                        XSIZE=6)
  

  ;; Stop and Go buttons
  commandblock = WIDGET_BASE(inblock_col1, /ROW)
  
  ;; GO button
  button = WIDGET_BUTTON(commandblock, VALUE='Calculate', UVALUE='CALC', $
                        xsize=103, ysize=115)

  ;; Save button
  button = WIDGET_BUTTON(commandblock, VALUE='Save', UVALUE='SAVE', $
                        xsize=103, ysize=115)
  
  ;; Reset button
  button = WIDGET_BUTTON(commandblock, VALUE='Reset', UVALUE='RESET', $
                        xsize=103, ysize=115)
  
  ;; Load button
  button = WIDGET_BUTTON(commandblock, VALUE='Load', UVALUE='LOAD', $
                        xsize=103, ysize=115)
  
  ;; Quit button
  button = WIDGET_BUTTON(commandblock, VALUE='Quit', UVALUE='QUIT', $
                        xsize=103, ysize=115)

  
  
  ;; Output tabs
  out_tabs=WIDGET_TAB(outblock, UVALUE='OUT_TAB_SEL')

  ;; Output summary block
  out_tab=WIDGET_BASE(out_tabs, TITLE='Calculation Summary', /COLUMN)
  out_text=WIDGET_TEXT(out_tab, VALUE='Make selections and hit the caluculate button to begin.', xsize=60, ysize=48, /WRAP)

  ;; Input summary block
  in_tab=WIDGET_BASE(out_tabs, TITLE='Details', /COLUMN)
  in_text=WIDGET_TEXT(in_tab, VALUE='', xsize=60, ysize=48, /WRAP)
  
  ;; Plot block
  plot_tab=WIDGET_BASE(out_tabs, TITLE='Spectrum Plot', /COLUMN)
  xsize=377 & ysize=212
  plot1=WIDGET_DRAW(plot_tab, xsize=xsize, ysize=ysize)
  plot2=WIDGET_DRAW(plot_tab, xsize=xsize, ysize=ysize)
  plot3=WIDGET_DRAW(plot_tab, xsize=xsize, ysize=ysize)

;  help_tab=WIDGET_BASE(out_tabs, TITLE='Help', /COLUMN)
;  help_text=WIDGET_TEXT(help_tab, xsize=60, ysize=49, /WRAP, $
;                        VALUE='Hover over a field and help will show up here.')
  
  
  widx={basein:inblock, baseout:outblock, full:full_draw, zoom:zoom_draw, $
        channel:channel_list, imgscl:[scl_lin, scl_log], $
        imgmin:imin_text, imgmax:imax_text, $
        xcoord:coordx_text, ycoord:coordy_text, angle:angle_text, $
        scat:qual_list, corona:corona_bgroup, $
        mosaicnl:mosaicnl_text, feed:feed_list, mosaicpat:scanpat_list, $
        mosaicnx:mosaicnx_text, mosaicny:mosaicny_text, $
        mosaicdx:mosaicdx_text, mosaicdy:mosaicdy_text, $
        ditherTF:dither_button, $
        ditherdx:ditherdx_text, ditherdy:ditherdy_text, $
        fido:fido_list, $
        wave1:wave_list1, wave2:wave_list2, wave3:wave_list3, $
        ircam:[ndr_button,sfr_button], $
        cam1:{ndr:ndr_text1, exp:exp_text1, coadd:cad_text1, dur:dur_text1, $
              plot:plot1}, $
        cam2:{ndr:ndr_text2, exp:exp_text2, coadd:cad_text2, dur:dur_text2, $
              plot:plot2}, $
        cam3:{ndr:ndr_text3, exp:exp_text3, coadd:cad_text3, dur:dur_text3, $
              plot:plot3}, $
        accum:accum_text, pol:[pol1, pol2, pol3], nstates:pol_text, $
        text_out:out_text, text_in:in_text}

end

;;-----------------------------------------------------------------------------

pro dlnirsp_ipc, restore=restorefile

  COMMON ipc_environment, img, widx, didx, iidx, dir
  COMMON dlnirsp_environment, input, output, tp, atlas, lines
  COMMON mouse, mouseread

  dir=routine_filepath('dlnirsp_ipc')
  dir=strmid(dir, 0, strpos(dir, 'dlnirsp_ipc.pro'))
  
  ;; make the user pick the file they want to restore
  if TYPENAME(restorefile) eq 'INT' then $
     restorefile=DIALOG_PICKFILE(FILTER='DLNIRSP_IPC_*.sav')
  
  ;; initialize DL-NIRSP input with default values
  DLNIRSP_IPC_INPUT, dir, RESTOREFILE=restorefile
  
  ;; determine image rescale factor for user display
  ss=GET_SCREEN_SIZE()
  nx=0.9*ss[0]
  ny=0.9*ss[1]

  scl=1.0

  nx=260
  ny=260
  
  ;; initialize image structure
  img={nx:nx, ny:ny, channel:'HMI Magnetogram'}

  
  ;; set mouse flags for display widget handler
  mouseread={flag:0, name:'', psym:0, color:0, $
             x:intarr(100), y:intarr(100), count:0}

  ;; store color and plot options that will be overridden
  font=!p.font
  DEVICE, GET_DECOMPOSED=decomp

  ;; override color and plot options for widget displays
  !p.font=-1
  DEVICE, DECOMPOSED=1
  DEVICE, RETAIN=2
  !x.margin=[0,0] & !y.margin=[0,0]
  
  ;; set up widget interfaces
  DLNIRSP_IPC_SETUP

  WIDGET_CONTROL, widx.basein, /REALIZE
  WIDGET_CONTROL, widx.baseout, /REALIZE
  
  DLNIRSP_IPC_UPDATE_IMG
  
  ;; get window indices for image and plot displays
  WIDGET_CONTROL, GET_VALUE=disp_full, widx.full
  WIDGET_CONTROL, GET_VALUE=disp_zoom, widx.zoom
  WIDGET_CONTROL, GET_VALUE=disp_spec1, widx.cam1.plot
  WIDGET_CONTROL, GET_VALUE=disp_spec2, widx.cam2.plot
  WIDGET_CONTROL, GET_VALUE=disp_spec3, widx.cam3.plot
  
  didx={full:disp_full, zoom:disp_zoom, font:font, decomp:decomp, $
        spec1:disp_spec1, spec2:disp_spec2, spec3:disp_spec3}
  
  DLNIRSP_IPC_UPDATE_DISP

  ;; set dropbox values
  DLNIRSP_IPC_SET_STATE
  
  XMANAGER, 'dlnirsp_ipc_widget', widx.basein, $
            EVENT_HANDLER='dlnirsp_ipc_event', /NO_BLOCK
  XMANAGER, 'dlnirsp_ipc_widget', widx.baseout, $
            EVENT_HANDLER='dlnirsp_ipc_event', /NO_BLOCK

end
