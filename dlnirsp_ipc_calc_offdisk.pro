;Calculate the intensity of the coronal lines and continuum as a function of
;radial distance and wavelength

;Input: wave - wavelength in [nm], can be an array
;       mu - cos(theta), should be a scalar
;       iscat - instrumental scattering state ['none','good','bad']
;
;Output: returns photons / sec / cm^2 / nm / str

function dlnirsp_ipc_calc_offdisk, wave, radius, line=line

  dir=routine_filepath('dlnirsp_ipc')
  dir=strmid(dir, 0, strpos(dir, 'dlnirsp_ipc.pro'))

  nwave=n_elements(wave)

  
  ;Get the average continuum intensity at disk center
  i0=dlnirsp_ipc_calc_disk(wave, 1.0) ;disk center photons at wavelengths
  
  
  ;sky intensity from Kimura & Mann (1998), Haleakala might be better
  fsky=1e-6
;  isky=i0*fsky

  
  ;coronal continuum intensity in disk center units
  restore, dir+'/data/coronal_intensities/Kimura_1998_Continuum_IvsR.sav'
  rdata=data
  
  restore, dir+'/data/coronal_intensities/Kimura_1998_Continuum_IvsW.sav'
  wdata=data
  
  fcont=interpol(rdata.i, rdata.r, radius) * $
        interpol(wdata.i/wdata.i[0], wdata.w, wave)

;  icont=i0*fcont

  ;Use line intensities taken from various references
  if keyword_set(line) ne 1 then begin
    
     iline=fltarr(nwave)
     dw=fltarr(nwave)
     w0=fltarr(nwave)

     for i=0,nwave-1 do begin
        case wave[i] of
           530.3D:begin
              print, 'no data for '+string(wave[i], format='(F6.1)')+' nm'
              data={w:530.3, dw:0.}
              iline[i]=0.
           end
        
           587.6D:begin
              print, 'no data for '+string(wave[i], format='(F6.1)')+' nm'
              data={w:587.6, dw:0.}
              iline[i]=0.
           end
        
           630.2D:begin
              print, 'no data for '+string(wave[i], format='(F6.1)')+' nm'
              data={w:630.2, dw:0.}
              iline[i]=0.
           end
        
           789.2D:begin
              print, 'no data for '+string(wave[i], format='(F6.1)')+' nm'
              data={w:789.2, dw:0.}
              iline[i]=0.
           end
           
           854.2D:begin
              print, 'no data for '+string(wave[i], format='(F6.1)')+' nm'
              data={w:854.2, dw:0.}
              iline[i]=0.
           end
     
           1074.7D:begin
              restore, dir+'/data/coronal_intensities/Penn_1994_10747_IvsR.sav'
              iline[i]=interpol(data.i, data.r, radius) ;this is the peak intensity
           end

           1079.89D:begin
              print, 'no data for '+string(wave[i], format='(F6.1)')+' nm'
              data={w:1079.89, dw:0.}
              iline[i]=0.
           end
        
           1082.95D:begin
              restore, dir+'/data/coronal_intensities/Kuhn_1996_10830_IvsR.sav'
              iline[i]=interpol(data.i, data.r, radius)
           end
     
           1430.0D:begin
              restore, dir+'/data/coronal_intensities/Penn_1994_14300_IvsR.sav'
              iline[i]=interpol(data.i, data.r, radius)
           end

           1565.05D:begin
              print, 'no data off-disk for '+string(wave[i], format='(F6.1)')+' nm'
              data={w:1565.0, dw:0.}
              iline[i]=0.
           end
        
        endcase
     
        w0[i]=mean(data.w)      ;average measured wavelength from source
     
     ;dw[i]=mean(data.dw)        ;average measured line width from source
        dw[i]=15./3e5*w0[i]     ; use 15 km/s line width
     
     endfor

  ;Use manually set line intensities
  endif else begin
     
     iline=line.i
     w0=wave + line.v/3e5*wave
     dw=line.dw/3e5*w0
     
  endelse
     
  ;put the line intensity together with the sky and  contribution to get the
  ;absolute intensity at the desired wavelengths, and convert to units
  ;of photons s-1
  cont=i0*fcont ;i0 is already in the right units

  sky=i0*fsky
  
;  line=i0*iline * $                         ; [J s-1 m-2 sr-1 Ang-1]
;       1e-4 * $                             ; m-2 to cm-2
;       10. * $                              ; Ang-1 to nm-1
;       (wave*1e-9) / (6.626069e-34 * 3e8)   ; photons / energy =
                                             ; lambda [m] / h [J sec] * c [m/s]
  line=i0*iline
  
  return, {line:{a:line, w:w0, dw:dw}, cont:cont, sky:sky}
 
end
