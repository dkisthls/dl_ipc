;+
; NAME:
;       dlnirsp_ipc_calc
;
; PURPOSE:
;       The Calculator part of the DL-NIRSP IPC
;
; CALLING SEQUENCE:
;       dlnirsp_ipc_calc
;
; INPUTS:
;       None, inputs are passed through common blocks
;
; OPTIONAL INPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       None 
;
; OUTPUTS:
;       None, outputs are passed through common blocks
;       
; OPTIONAL OUTPUTS:
;
; COMMON BLOCKS:
;       dlnirsp_environment
;       ipc_environment
;
; PROCEDURES USED:
;       
;       Contained in this file:
;          dlnirsp_ipc_calc_phot - does the physical calculation of
;                                  photons at the detector
;          dlnirsp_ipc_calc_snr  - calculates the signal and noise
;                                  given camera aquisition paramerts
;          dlnirsp_ipc_calc_data - calculates data rates and times
;                                  based on exposure, modulation, and
;                                  mosaic settings
;          dlnirsp_ipc_calc_mosaic -  calculates the mosaic positions
;                                     and scan order (maybe someday)
;
; COMMENTS:
;
; EXAMPLES:
;          IDL> dlnirsp_ipc_calc
;
; MODIFICATION HISTORY:
;       Started 2016-Aug-25 by Sarah A. Jaeggli, National Solar
;       Observatory
;               2017-Oct-09 SAJ implemented inverse calculation which
;               determines observation times and coadds based on desired
;               signal to noise.
;               2018-Mar-08 SAJ fixed error with how the fiber filling
;               factor was implemented and removed redundant filling
;               factor coming from the radiometry calculation.  Added
;               an assumed rebin to the imaging pixel size (another
;               factor of sqrt 2 or 3 improvement in noise on disk,
;               sqrt 4 or 6 off disk).  Updated this calculation to
;               use the actual expected dispersions from spectrograph
;               geometry calculation.
;               2018-03-13 SAJ changed assumption of how much to fill
;               detector fullwell to 50% based on study of pixel/fiber
;               aliasing.
;               2018-03-22 SAJ fixed fiber filling factor again, was
;               only using the fiber core width instead of the full
;               width of the fiber.  The integration time can only be an
;               integer multiple of the frame rate so long as it is
;               longer than the frame rate.  Added constraints to the forward
;               calculation, using user-set priority line to set the
;               exposure and coadd for all other channels.
;               2022-05-24 SAJ made major changes, see note in
;               dlnirsp_ipc.pro
;               2023-01-23 SAJ added "reset_time" to mosaic
;               structure for mosaic to match ~8 sec wait time between
;               mosaic repeats.
;               2024-03-11 SAJ Major update for MISIs and IR cameras sub-
;               frametime mode
;               2024-03-28 SAJ removed calculation of exposure times based on
;               signal to noise and other references to advanced mode
;------------------------------------------------------------------------------

;;-----------------------------------------------------------------------------
;; calculate number of photons incident per pixel per second based on
;; telescope and instrument parameters
PRO dlnirsp_ipc_calc_phot, wave, cam, mult

  COMMON dlnirsp_environment, input, output, tp, atlas, lines
  
  ;; some constants
  c=3e8                         ;m sec-1
;  cm_per_AU=1.49659787e13
;  rsun_cm=6.957e10
;  dsun=1.0*cm_per_AU
;  rsun=atan(rsun_cm/dsun)*180.*3600./!pi ;in arcsec
  
  ifu=input.ifu[input.selected.ifu]
  feed=input.feed[input.selected.feed]
  rsun=input.target.rsun
  
  ;; calculate the radial distance
  robs=sqrt(input.target.x^2 + input.target.y^2)/rsun
  mu=cos(asin(robs))
  if robs lt 1.0 then ondisk=1 else ondisk=0
  
  
  ;; calculate telescope diffraction limit at wavelength
  diff=1.22*wave.l0/1e7/input.tele.diam ;convert wavelength to cm
  diff=diff/!pi*180.*3600. ;convert to arcsec from radians
  
  ;; Determine photons collected by telescope area
  rad=input.tele.diam/2.        ;radius [cm]
  tele_area=!pi*rad^2           ;in cm^2
  
  ;; str to arcsec
  arc_per_rad=3600.*180./!pi
  
  m1=tele_area/arc_per_rad^2  ;*i0 [photons s-1 arcsec-2 nm-1] collected by aperture


  ;; select the right beamsplitter configuration for throughput
  case input.fido[input.selected.fido].name of
     'IR only' : tpbs=tp.bs_conf2
     'Visible and IR' : tpbs=tp.bs_conf1
  endcase

  ;; select the right throughput for the feed optics mode and IFU
  case input.selected.feed of
     0 : tpfeed=tp.f62
     1 : tpfeed=tp.f24
     2 : tpfeed=tp.f24
  endcase

  case input.selected.ifu of
     0 : tpifu=tp.misi36
     1 : tpifu=tp.misi116
  endcase

  ;; interpolate the throughput table for this wavelength
  tptotal=tpifu*tpfeed*tpbs*tp.tele*tp.spec
  throughput=interpol(tptotal, tp.wavelength, wave.l0)
  
  
  ;; [photons s-1 arcsec-2 nm-1] that make it to the detector
  ;; extra factor for the polarizing beam splitter
  m2=m1*throughput/input.pol.nbeams

  
  ;; dispersion [nm/pixel] at detector
  dl=cam.pitch/(wave.dxdl*1e3)

  ;; total wavelength range possible on detector
  xsize=18.*2048. / (input.spec.nslits*input.pol.nbeams) / cam.pitch ;in pixels
  
  l0=wave.l0 - dl*xsize/2.
  l1=wave.l0 + dl*xsize/2.
  
  m3=m2*dl                    ;[photons s-1 arcsec-2 spectral pixel-1]


  ;; width subtended by the fiber at the image plane [arcsec/xpixel]
  dx=(ifu.fiber.x +2.*ifu.fiber.c)*ifu.fiber.nfbr[0] * feed.ps
  
  m4=m3*dx                    ;[photons s-1 arcsec-1 xpixel-1 spectral pixel-1]

  
  ;; fiber sample spacing
  dy1=(ifu.fiber.y + 2.*ifu.fiber.c) * feed.ps

  ;; pixel sampling
  dy2=cam.pitch * feed.ps
  
  m5=m4*dy2                   ;[photons s-1 ypixel-1 xpixel-1 spectral pixel-1]


  mult=m5
  
  
  ;; put parameters in output structure
  output.signal[cam.idx].eff=throughput
  output.signal[cam.idx].dx=dx
  output.signal[cam.idx].dy_cam=dy2
  output.signal[cam.idx].dy_fib=dy1
  output.signal[cam.idx].diff=diff
  output.signal[cam.idx].dl=dl

  output.wave[cam.idx].l0=wave.l0
  output.wave[cam.idx].dl=dl
  output.wave[cam.idx].ran[0]=l0
  output.wave[cam.idx].ran[1]=l1
  output.wave[cam.idx].vel=(l1/wave.l0-1.)*c*1e9
          
  output.target.ondisk=ondisk
  output.target.robs=robs
  output.target.mu=mu

  output.mosaic.tilex=ifu.iput.sx * feed.ps
  output.mosaic.tiley=ifu.iput.sy * feed.ps
  output.mosaic.fovx=((input.mosaic.nx - 1) * input.mosaic.dx + 1) * $
                     ifu.iput.sx * feed.ps
  output.mosaic.fovy=((input.mosaic.ny - 1) * input.mosaic.dy + 1) * $
                     ifu.iput.sy * feed.ps
  
END


;;-----------------------------------------------------------------------------
;; Calculate the signal and noise per pixel
PRO dlnirsp_ipc_calc_snr, wave, cam, phot, snr
  COMMON dlnirsp_environment, input, output, tp, atlas, lines

  feed=input.feed[input.selected.feed]
  
  ;; science signal count rates [e-/sec]
  phot_rate=phot.source

  ;; background counts are scattered light + sky + coronal contiuum [e-/sec]
  back_rate=phot.scat+phot.sky+phot.cont

  ;; dark counts include dark current and thermal background [e-/sec]
  dark_rate=cam.dc+cam.tb

  ;; bias signal, does not contribute to noise but does contribute to
  ;; ADU saturation level
  bias_signal=cam.bi ;[e- ypixel-1 xpixel-1 spectral pixel-1]

  ;; the ADC full well is based on the bits per pixel [e-]
  adc_fw=(2L)^cam.bits_per_pixel*cam.gain - bias_signal

  
  ;; photon shot noise
  phot_signal = phot_rate*cam.exp ;[photons ypixel-1 xpixel-1 spectral pixel-1]
  phot_noise = sqrt(phot_signal)
  
  ;; background signal shot noise
  back_signal = back_rate*cam.exp ;[photons ypixel-1 xpixel-1 spectral pixel-1]
  back_noise = sqrt(back_signal)
  
  ;; dark shot noise
  dark_signal = dark_rate*cam.exp ;[photons ypixel-1 xpixel-1 spectral pixel-1]
  dark_noise = sqrt(dark_signal)
  
  ;; read noise [e-]
  read_noise = cam.rn
  
  ;; quantization/digitization noise [e-]
  quan_noise = 0.288675*cam.gain
  
  ;; noise with non-destructive reads
;  fac_ndr=1./sqrt(cam.ndr)
  fac_ndr = 1. ;I don't know how noise works in non-destructive reads

  
  ;; post-processing noise factors (keeping normalized to counts in a single
  ;; frame)

  ;; noise after demodulation of polarization???
  fac_pol = 1./sqrt(input.pol.nstates)

  ;; factor for combining dual beam
  fac_beam = 1./sqrt(input.pol.nbeams)
  
  ;; noise factor for rebinning from detector pixels to "imaging pixels"
  fac_rebin = 1./sqrt(ceil(output.signal[cam.idx].dx / (cam.pitch*feed.ps)))
     
  ;; noise factor for coadding and accumulating
  fac_coadd = 1./sqrt(cam.coadd+input.pol.accum)

  
  ;; Noise total normalized to counts from a single frame
  total_noise = sqrt(phot_noise^2 + back_noise^2 + dark_noise^2 + $
                     read_noise^2 + quan_noise^2) * $
                (fac_ndr*fac_pol*fac_beam*fac_rebin*fac_coadd)


  ;; check if the counts exceed the detector full well in an individual image
  sat = where((phot_signal + back_signal + dark_signal) / cam.fw gt 1.0, count)
  is_saturated = wave.l0*0
  if count gt 0 then is_saturated[sat] = 1

  
  ;; SNR for the quiet-Sun continuum
  snr = phot_signal/total_noise

 
  ;; put parameters in output structure
  output.signal[cam.idx].phot = phot_signal
  output.signal[cam.idx].back = back_signal
  output.signal[cam.idx].dark = dark_signal
  output.signal[cam.idx].bias = bias_signal
  output.signal[cam.idx].snr = snr
  output.signal[cam.idx].sat = is_saturated
  
  output.noise[cam.idx].phot = phot_noise * (fac_ndr*fac_coadd*fac_pol)
  output.noise[cam.idx].dark = dark_noise * (fac_ndr*fac_coadd*fac_pol)
  output.noise[cam.idx].read = read_noise * (fac_ndr*fac_coadd*fac_pol)
  output.noise[cam.idx].quan = quan_noise * (fac_ndr*fac_coadd*fac_pol)
  output.noise[cam.idx].total = total_noise
  
END

;;-----------------------------------------------------------------------------
;; calculate the resulting data size and rates, resulting units are [sec, MB]
PRO dlnirsp_ipc_calc_data, cam
  COMMON dlnirsp_environment, input, output, tp, atlas, lines

  ;; constants
;  bits_per_MB=8.*2.^20  
  bits_per_MB = 8e6

  ;; total time for each image frame
  time_per_frame = max( (cam.duty[0] + cam.duty[1]*cam.ndr)/cam.fr*cam.coadd )
  
  ;; discrete modulator moves require additional time
  if input.selected.pol eq 2 then time_per_frame+=input.pol.move_time

  
  ;; time per tile
  time_per_tile = time_per_frame * input.pol.nstates*input.pol.accum
  
  ;; mosaic has an assumed wait time, even if it's not moving
  time_per_tile+=input.mosaic.move_time

  
  ;; time per mosaic (with dither)
  time_per_mosaic = input.mosaic.reset_time + time_per_tile * $
                    input.mosaic.nx*input.mosaic.ny*(1.+input.selected.dither)
  
  mosaic_duty = cam.exp*cam.coadd*input.pol.nstates*input.pol.accum / $
                time_per_tile


  ;; time per total observations
  time_per_obs = time_per_mosaic * input.mosaic.nl

  
  ;; data quantity in [MB]
  ;;frames per coadd and accumulation cycle
  frames_per_coadd=(1+cam.ndr) * cam.coadd * input.pol.accum
  if cam[0].idx eq 0 then frames_per_coadd[0]=1
  
  data_per_tile=total(cam.bits_per_pixel * cam.nx * cam.ny * $
                      input.pol.nstates * frames_per_coadd) / bits_per_MB
 
  data_per_mosaic=data_per_tile * input.mosaic.nx * input.mosaic.ny * $
                  (1.+input.selected.dither)
  data_per_obs=data_per_mosaic * input.mosaic.nl

  ;; data rate in [MB/sec]
  data_per_time=float(data_per_obs) / time_per_obs

  
  ;; put parameters in output structure
  output.time.frame=time_per_frame
  output.time.tile=time_per_tile
  output.time.mosaic=time_per_mosaic
  output.time.obs=time_per_obs
  
  output.data.tile=data_per_tile
  output.data.mosaic=data_per_mosaic
  output.data.obs=data_per_obs
  
  output.data.rate=data_per_time
END


PRO dlnirsp_ipc_calc_mosaic
;; Calculate the positions of every tile in the mosaic relative to the center
;; of the field
END


;;-----------------------------------------------------------------------------
;; Main wrapper for calculations
PRO dlnirsp_ipc_calc

  COMMON dlnirsp_environment, input, output, tp, atlas, lines

  wave=[input.wave1[input.selected.wave1], $
        input.wave2[input.selected.wave2], $
        input.wave3[input.selected.wave3]]

  cam=[input.cam1, input.cam2, input.cam3]
  
  scat=input.inten.scat.val[input.selected.scat]
  
  wave_sel=[input.selected.wave1, input.selected.wave2, input.selected.wave3]
  
  good_wave=where(wave_sel gt 0, count)

  if count eq 0 then begin
     print, 'No lines were selected for the calculation.  ' + $
            'Select lines and try again.'
  endif
  
  wave=wave[good_wave]
  cam=cam[good_wave]

  
  ;; get the conversion factor from [photons s-1 sr-1 cm-2 nm-1] to 
  ;; [photons s-1 pixel-1] for setup
  dlnirsp_ipc_calc_phot, wave, cam, mult

  i0=dlnirsp_ipc_calc_disk(wave.l0, 1.0) ;disk center photons at wavelengths
  input.inten.sun0[cam.idx]=dlnirsp_ipc_calc_disk(wave.l0, 1.0, /cgs)
  
  ;; if on-disk then get solar disk intensity from limb darkening
  if output.target.ondisk then begin
     i1=dlnirsp_ipc_calc_disk(wave.l0, output.target.mu)
     
     phot={source:i1 * mult, $  ;source brightness
             scat:i0 * scat * mult, $
              sky:0. * i0, $
             cont:0. * i0}

     input.inten.cont[cam.idx]=i1 / i0
     
     for w=0,count-1 do begin
        case cam[w].idx of
           0:spectrum=output.spectrum1
           1:spectrum=output.spectrum2
           2:spectrum=output.spectrum3
        endcase
     
        nl=n_elements(spectrum.l)
        wavel=output.wave[cam[w].idx].ran[0] + output.wave[cam[w].idx].dl * $
              dindgen(nl) < output.wave[cam[w].idx].ran[1]

        spec0=interpol(atlas.i, atlas.w, wavel)

        isource=spec0*phot.source[w] / (i0[w]*mult[w])
        iscat=spec0*phot.scat[w] / (i0[w]*mult[w])
        isky=spec0*phot.sky[w] / (i0[w]*mult[w])
        icont=spec0*0.+phot.cont[w] / (i0[w]*mult[w])

        spectrum.i=isource+iscat+isky+icont
        spectrum.isource=isource
        spectrum.iscat=iscat
        spectrum.isky=isky
        spectrum.icont=icont
        spectrum.l=wavel

        case cam[w].idx of
           0:output.spectrum1=spectrum
           1:output.spectrum2=spectrum
           2:output.spectrum3=spectrum
        endcase
     endfor
     
  ;; if off-disk then get coronal and scattered light intensities
  endif else begin

     if input.selected.corona eq 1 then begin
        i1=dlnirsp_ipc_calc_offdisk(wave.l0, output.target.robs, $
                                    line=input.inten.line[good_wave])
     endif else begin
        i1=dlnirsp_ipc_calc_offdisk(wave.l0, output.target.robs)

        input.inten.cont[cam.idx]=(i1.sky + i1.cont) / i0
        input.inten.line[cam.idx].i=i1.line.a / i0
        input.inten.line[cam.idx].v=(i1.line.w-wave.l0)/wave.l0 * 3e5
        input.inten.line[cam.idx].dw=i1.line.dw/wave.l0 * 3e5
     endelse
     
     phot={source:i1.line.a * mult, $
             scat:i0 * scat * mult, $
              sky:i1.sky * mult, $
             cont:i1.cont * mult}

     for w=0,count-1 do begin
        case cam[w].idx of
           0:spectrum=output.spectrum1
           1:spectrum=output.spectrum2
           2:spectrum=output.spectrum3
        endcase
     
        nl=n_elements(spectrum.l)
        wavel=output.wave[cam[w].idx].ran[0] + output.wave[cam[w].idx].dl * $
              dindgen(nl) < output.wave[cam[w].idx].ran[1]

        spec0=interpol(atlas.i, atlas.w, wavel)

        isource=phot.source[w]*exp(-0.5*((wavel-i1.line.w[w])/i1.line.dw[w])^2)$
                / (i0[w]*mult[w])
        iscat=spec0*phot.scat[w] / (i0[w]*mult[w])
        isky=spec0*phot.sky[w] / (i0[w]*mult[w])
        icont=spec0*0.+phot.cont[w] / (i0[w]*mult[w])

        spectrum.i=isource+iscat+isky+icont
        spectrum.isource=isource
        spectrum.iscat=iscat
        spectrum.isky=isky
        spectrum.icont=icont
        spectrum.l=wavel

        case cam[w].idx of
           0:output.spectrum1=spectrum
           1:output.spectrum2=spectrum
           2:output.spectrum3=spectrum
        endcase
     endfor
          
  endelse
  ;; intensities are in units of [photons s-1 pixel-1]
  
  
  ;; get SNR based on camera parameters or get camera parameters based on SNR
  dlnirsp_ipc_calc_snr, wave, cam, phot, snr


  ;; now do the data calculation
  dlnirsp_ipc_calc_data, cam

  
  ;; now do the mosaic calculation
;  dlnirsp_ipc_calc_mosaic
  

  ;; load parameters back
  for i=0,count-1 do begin
     case good_wave[i] of
        0:input.cam1=cam[i]
        1:input.cam2=cam[i]
        2:input.cam3=cam[i]
     endcase
  endfor
  
end
