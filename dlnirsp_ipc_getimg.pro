;+
; NAME:
;       dlnirsp_ipc_getimg
;
; PURPOSE:
;       A function that loads image and necessary header information
;       from a default set of SDO fits images or from a properly formatted
;       user image
;
; CALLING SEQUENCE:
;       dlnirsp_ipc_getimg, channel, index
;
; INPUTS:
;       channel  - A string containing the channel name 
;
; OPTIONAL INPUTS:
;       None
;
; KEYWORD PARAMETERS:
;       None 
;
; OUTPUTS:
;       data     - array with image data
;       index    - structure with metadata parameters
;
; OPTIONAL OUTPUTS:
;       None
;
; COMMON BLOCKS:
;       None
;
; PROCEDURES USED:
;       readfits and fxpar from AstroLib, if you don't have
;       this in your IDL library, I don't know what you think
;       you're doing.
;
; COMMENTS:
;
; EXAMPLES:
;          IDL> data=dlnirsp_ipc_getimg('HMI Magnetogram', index)
;
; MODIFICATION HISTORY:
;       Started 2017-Oct-04 by Sarah A. Jaeggli, National Solar
;       Observatory
;
;-

;------------------------------------------------------------------------------

function dlnirsp_ipc_getimg, channel, index, dir0

  rsun=943.87103655849035 ;r_sun in arcsec at time of SDO images
  
  case channel of
     'HMI Magnetogram':begin
        file='./data/SDO/hmi.m_45s.2014.07.07_00-01-30_TAI.magnetogram.fits'
        log=0 & iran=[-400,400]
        reverse=1
        dir=dir0
     end
   
     'HMI Intensitygram':begin
        file='./data/SDO/hmi.ic_45s.2014.07.07_00-01-30_TAI.continuum.fits'
        log=0 & iran=[0,70000]
        reverse=1
        dir=dir0
     end
   
     'AIA 1700':begin
        file='./data/SDO/aia.lev1.1700A_2014-07-07T00-00-30.71Z.image_lev1.fits'
        log=1 & iran=[1.5,3.5]
        reverse=0
        dir=dir0
     end
   
     'AIA 304':begin
        file='./data/SDO/aia.lev1.304A_2014-07-07T00-00-07.12Z.image_lev1.fits'
        log=1 & iran=[0.5,3.0]
        reverse=0
        dir=dir0
     end
   
     'AIA 193':begin
        file='./data/SDO/aia.lev1.193A_2014-07-07T00-00-06.84Z.image_lev1.fits'
        log=1 & iran=[1.5,4.0]
        reverse=0
        dir=dir0
     end
   
     'AIA 171':begin
        file='./data/SDO/aia.lev1.171A_2014-07-07T00-00-11.34Z.image_lev1.fits'
        log=1 & iran=[1.5,3.5]
        reverse=0
        dir=dir0
     end

     'User Input':begin
        file=DIALOG_PICKFILE(FILTER='*.fits')
        rsun=950.               ;average r_sun?
        log=0
        reverse=0
        dir=''
     end
   
  endcase
  
  data=readfits(dir+file, hdr)

  if reverse then data=rotate(data, 2)
  if keyword_set(iran) ne 1 then iran=[min(data),max(data)]
  
  index={naxis1:fxpar(hdr, 'NAXIS1'), naxis2:fxpar(hdr, 'NAXIS2'), $
         cdelt1:fxpar(hdr, 'CDELT1'), cdelt2:fxpar(hdr, 'CDELT2'), $
         crpix1:fxpar(hdr, 'CRPIX1'), crval1:fxpar(hdr, 'CRVAL1'), $
         crpix2:fxpar(hdr, 'CRPIX2'), crval2:fxpar(hdr, 'CRVAL2'), $
         rsun:rsun, iran:iran, log:log}
  
  return, data
  
end
