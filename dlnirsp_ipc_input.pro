;+
; NAME:
;       dlnirsp_ipc_input
;
; PURPOSE:
;       Hard-coded instrument parameters and defaults for the
;       DKIST/DL-NIRSP system, data retrieval, and setup of output
;       buffers.
;
; CALLING SEQUENCE:
;       dlnirsp_ipc_input, dir
;
; INPUTS:
;       dir - the routine directory, passed from DLNIRSP_IPC, which
;             the data directory is located within
;
; OPTIONAL INPUTS:
;       restorefile - a file with previously generated input and
;                     output structures
;
; KEYWORD PARAMETERS:
;       None
;
; OUTPUTS:
;       None, outputs are passed through common blocks
;       
; OPTIONAL OUTPUTS:
;
; COMMON BLOCKS:
;       dlnirsp_environment
;
; PROCEDURES USED:
;       
; COMMENTS:
;
; EXAMPLES:
;          IDL> dlnirsp_ipc_input, dir
;
; MODIFICATION HISTORY:
;       Started 2016-Aug-25 by Sarah A. Jaeggli, National Solar
;       Observatory
;               2018-Jan-09 updated camera paramenters
;               2018-Mar-08 SAJ added expected spectral dispersions
;               for spectrograph. Updated wavelengths to include all
;               the filters we are currently purchasing.
;               2022-05-24 SAJ made major changes, see note in
;               dlnirsp_ipc.pro
;               2023-01-23 SAJ added date for IPC version in the input
;               data structure.  Added "reset_time" to mosaic
;               structure for mosaic to match ~8 sec wait time between
;               mosaic repeats.
;               2024-02-08 SAJ updated parameters for MISI-36 and edited
;               feed parameters for new IFUs.
;               2024-03-28 SAJ added exposure parameter presets and removed
;               things related to advanced mode and line priority.
;------------------------------------------------------------------------------

PRO dlnirsp_ipc_input, dir, restorefile=restorefile

  common dlnirsp_environment, input, output, tp, atlas, lines
  
  if keyword_set(restorefile) then restore, restorefile else begin
  
     input={version_date:'2024-03-28', $

            target:{x:0., y:0., angle:0., $
                    date_obs:'2014-07-07T00:00:00', $
                    rsun:950.0}, $ ;solar radius in arcsec
         
            ;; Telescope
            tele:{diam:400.}, $ ;telescope aperture in [cm]
            
                   ;; scattered light intensity
            inten:{scat:{qual:['none', 'good', 'bad '], $
                         val:[0.0, 25e-6, 160e-6]}, $ ;telescope scattered light

                   ;; manual coronal line parameters
                   line:[{i:10e-6, v:0., dw:15.}, $
                         {i:10e-6, v:0., dw:15.}, $
                         {i:10e-6, v:0., dw:15.} ], $

                   ;; continuum signal (photospheric signal on-disk, coronal
                   ;; continuum off-disk)
                   cont:[0.0, 0.0, 0.0], $
     
                   ;; solar disk center intensity [erg s-1 cm-2 sr-1 nm-1]
                   sun0:[0.0, 0.0, 0.0] }, $
            
            mosaic:{nl:1L, nx:1L, ny:1L, $
                    dx:1.0, dy:1.0, ddx:0.5, ddy:0.5, $
                    move_time:0.6, reset_time:7.65, $
                    pat:['Snake', 'Zig-zag', 'Spiral']}, $

            
            ;; Feed parameters, focal ratio, plate scale in "/um (calculated
            ;; below) and max field of view in arcsec
;            feed:[{name:'High f/62 BiFOIS-36', fr:62., ps:0., fov:2.8*60.}, $
;                  {name:'Mid  f/24 BiFOIS-36', fr:24., ps:0., fov:2.8*60.}, $
;                  {name:'Wide f/8  BiFOIS-72', fr: 8., ps:0., fov:2.8*60.}], $

            feed:[{name:'High f/62 MISI-36  ', fr:62., ps:0., fov:2.8*60.}, $
                  {name:'Mid  f/24 MISI-36  ', fr:24., ps:0., fov:2.8*60.}, $
                  {name:'Wide f/12 MISI-116 ', fr:12., ps:0., fov:2.8*60.}],$

            
            ;; IFU parameters
            ifu:[{name:'MISI-36', $
                   
                  iput:{nrib:56, $     ;number of ribbons(slitlets) in stack
                        nstk:1, $      ;number of stacks
                        ribsep:0., $   ;separation between stacked ribbons [um]
                        stksep:0., $   ;separation of ribbon stacks [um]
                        sx:0., $       ;xsize of input [um] calculated below
                        sy:0.}, $      ;ysize of input [um]
                   
                  oput:{nrib:28, $     ;number of ribbons per slit
                        nslt:4, $      ;number of slits
                        ribsep:76., $  ;separation between ribbons in slit [um]
                        sltsep:8640.}, $ ;separation between slits [um]
                   
                  fiber:{x:36., $      ;long dimension of fiber core [um]
                         y:36., $      ;short dimension of fiber core [um]
                         c:0.0, $      ;cladding thickness [um]
                         nfbr:[1,60]} }, $ ;number of fibers in ribbon

                 {name:'MISI-116', $
                   
                  iput:{nrib:54, $     ;number of ribbons in stack
                        nstk:1, $      ;number of stacks
                        ribsep:0., $   ;separation between stacked ribbons [um]
                        stksep:0., $   ;separation of ribbon stacks [um]
                        sx:0., $       ;xsize of input [um] calculated below
                        sy:0.}, $      ;ysize of input [um]
                   
                  oput:{nrib:14, $     ;number of ribbons per slit
                        nslt:4, $      ;number of slits
                        ribsep:76., $  ;separation between ribbons in slit [um]
                        sltsep:8640.}, $ ;separation between slits [um]
                   
                  fiber:{x:116., $     ;long dimension of fiber core [um]
                         y:116., $     ;short dimension of fiber core [um]
                         c:0.0, $      ;cladding thickness [um]
                         nfbr:[1,20]} }, $ ;number of fibers in ribbon

            {name:'BiFOIS-36', $
                  
                  iput:{nrib:64, $     ;number of ribbons in stack
                        nstk:2, $      ;number of stacks
                        ribsep:0., $   ;separation between stacked ribbons [um]
                        stksep:0., $   ;separation of ribbon stacks [um]
                        sx:0., $       ;xsize of input [um] calculated below
                        sy:0.}, $      ;ysize of input [um]
                  
                  oput:{nrib:32, $     ;number of ribbons per slit
                        nslt:4, $      ;number of slits
                        ribsep:76., $  ;separation between ribbons in slit [um]
                        sltsep:8640.}, $ ;separation between slits [um]
                  
                  fiber:{x:29., $      ;long dimension of fiber core [um]
                         y:5., $       ;short dimension of fiber core [um]
                         c:3.5, $      ;cladding thickness [um]
                         nfbr:[1,90]} }, $ ;number of fibers in ribbon
     
                 {name:'BiFOIS-72', $
                   
                  iput:{nrib:32, $     ;number of ribbons in stack
                        nstk:2, $      ;number of stacks
                        ribsep:0., $   ;separation between stacked ribbons [um]
                        stksep:0., $   ;separation of ribbon stacks [um]
                        sx:0., $       ;xsize of input [um] calculated below
                        sy:0.}, $      ;ysize of input [um]
                   
                  oput:{nrib:16, $     ;number of ribbons per slit
                        nslt:4, $      ;number of slits
                        ribsep:76., $  ;separation between ribbons in slit [um]
                        sltsep:8640.}, $ ;separation between slits [um]
                   
                  fiber:{x:29., $      ;long dimension of fiber core [um]
                         y:5., $       ;short dimension of fiber core [um]
                         c:3.5, $      ;cladding thickness [um]
                         nfbr:[2,180]} }], $ ;number of fibers in ribbon
                  
            ;; Spectrograph
            spec:{lines_per_mm:23.2,  $ ;grating constant [lines/mm]
                  blaze_angle:63.0,   $ ;blaze angle [deg]
                  focal_length:1250., $ ;focal length of spectrograph camera [mm]
                  nslits:4.}, $

            ;; units are                         [nm]          [mm/nm]
            wave1:[{name:'(none)           ', l0:0.0D,    dxdl:0.0D,    type:''}, $
                   {name:'Fe XIV   530.3 nm', l0:530.30D, dxdl:8.6151D, type:'ch'}, $
                   {name:'He I D3  587.6 nm', l0:587.60D, dxdl:7.8382D, type:'ch'}, $
                   {name:'Fe I     630.2 nm', l0:630.20D, dxdl:7.2430D, type:'ph'}, $
                   {name:'Fe XI    789.2 nm', l0:789.20D, dxdl:5.6037D, type:'co'}, $
                   {name:'Ca II    854.2 nm', l0:854.20D, dxdl:5.3403D, type:'ch'}], $
            
            wave2:[{name:'(none)           ', l0:0.0D,     dxdl:0.0D,    type:''}, $
                   {name:'Fe XIII 1074.7 nm', l0:1074.70D, dxdl:4.0231D, type:'co'}, $
                   {name:'Fe XIII 1079.8 nm', l0:1079.89D, dxdl:4.1415D, type:'co'}, $
                   {name:'He I    1083.0 nm', l0:1082.95D, dxdl:4.2170D, type:'ch'}], $
            
            wave3:[{name:'(none)           ', l0:0.0D,     dxdl:0.0D,    type:''}, $
                   {name:'Si X    1430.0 nm', l0:1430.00D, dxdl:3.3017D, type:'co'}, $
                   {name:'Fe I    1565.0 nm', l0:1565.05D, dxdl:2.8618D, type:'ph'}], $
      
            ;; Cameras
            ircam_mode:[{name:'NDR Up-the-Ramp'}, $
                        {name:'SFR Sub-Frametime'}], $
     
            ;; Andor Balor visible camera
            cam1:{name:'Balor', $
                  idx:0, $ ;index for housekeeping purposes
                  exp:0.0, ndr:1L, sfr:1023L, coadd:1L, $ ;User settings
                  snr:100L, pri:0, $ 
                  fr:29.4,   $  ;frame rate in Hz
                  nc:32,     $  ;number of readout channels
                  fw:8.5e4,  $  ;full well [e-/pixel]
                  rn:3.0,    $  ;read noise [e-/pixel]
                  dc:0.02,   $  ;dark current [e-/sec/pixel]
                  tb:1.6e4,  $  ;thermal background [e-/sec/pixel]
                  bi:0.0,    $  ;detector bias [e-/pixel]
                  gain:1.4,  $  ;[e-/DN]
;                  nx:4128.,  $  ;detector format
;                  ny:4104.,  $
                  nx:3072.,  $  ;detector format with windowing
                  ny:3072.,  $
                  pitch:12., $       ;pixel size [microns]
                  xsize:12.*3072., $ ;total size of detector
                  ysize:12.*3072., $
                  duty:[1.0, 1.0], $ ;duty cycle duty[0]*fr+ndr*fr*duty[1]
                  bits_per_pixel:16.}, $

            ;; Hawaii2RG
            cam2:{name:'H2RG1', $
                  idx:1, $ ;index for housekeeping purposes
                  exp:0.0, ndr:1L, sfr:1023L, coadd:1L, $ ;User settings
                  snr:100L, pri:0, $
                  fr:29.4,   $  ;frame rate in Hz
                  nc:32,     $  ;number of readout channels
                  fw:1.8e5,  $  ;full well, e-/pixel
                  rn:80.,    $  ;read noise, e-/pixel
                  dc:0.0,    $  ;dark current, e-/sec/pixel
                  tb:7.2e5,  $  ;thermal background, e-/sec/pixel
                  bi:4.4e4,  $  ;detector bias [e-/pixel]
                  gain:4.44, $  ;[e-/ADU] (2 e-/ADU is the quoted minimum gain)
                  nx:2048.,  $  ;detector format
                  ny:2048.,  $
                  pitch:18., $    ;pixel size [microns]
                  xsize:38000., $ ;total size of detector [microns]
                  ysize:38000., $
                  duty:[1.0,1.0], $ ;duty cycle duty[0]*fr+ndr*fr*duty[1]
                  bits_per_pixel:16.}, $

            ;; Hawaii2RG
            cam3:{name:'H2RG2', $
                  idx:2, $ ;index for housekeeping purposes
                  exp:0.0, ndr:1L, sfr:1023L, coadd:1L, $  ;User settings
                  snr:100L, pri:0, $
                  fr:29.4,   $  ;frame rate in Hz
                  nc:32,     $  ;number of readout channels
                  fw:1.8e5,  $  ;full well, e-/pixel
                  rn:80.,    $  ;read noise, e-/pixel
                  dc:0.0,    $  ;dark current, e-/sec/pixel
                  tb:5.2e5,  $  ;thermal background, e-/sec/pixel
                  bi:6.2e4,  $  ;detector bias [e-/pixel]
                  gain:4.44, $  ;[e-/ADU]
                  nx:2048.,  $  ;detector format
                  ny:2048.,  $
                  pitch:18., $  ;pixel size [microns]
                  xsize:38000., $ ;total size of detector [microns]
                  ysize:38000., $
                  duty:[1.0,1.0], $   ;duty cycle
                  bits_per_pixel:16.}, $

            ;; Beamsplitter Configuration
            fido:[{name:'IR only'}, $
                  {name:'Visible and IR'}], $

            ;; Polarization Parameters
            pol:{mode:['Off', 'Continuous','Discrete'], $
                 nstates:8, nbeams:2., move_time:1./29.4, accum:2L}, $

            ;; Selection Flags   
            selected:{scat:0, corona:0, $
                      feed:0, ifu:0, pat:0, dither:0, $
                      wave1:5, wave2:3, wave3:2, $
                      ircam:0, fido:1, pol:1}, $

            ;; Camera presets
            presets:[[{ircam:0, $ ;on-disk, high-res mode
                       cam1:{ndr:1L, exp:0.0, coadd:1}, $
                       cam2:{ndr:1L, exp:0.0, coadd:1}, $
                       cam3:{ndr:1L, exp:0.0, coadd:1}}, $
                      {ircam:1, $ ;on-disk, mid-res mode
                       cam1:{ndr:0L, exp:0.0195, coadd:3}, $
                       cam2:{ndr:1271L, exp:0.0, coadd:3}, $
                       cam3:{ndr:571L, exp:0.0, coadd:3}}, $
                      {ircam:1, $ ;on-disk, wide field mode
                       cam1:{ndr:0L, exp:0.00155, coadd:3}, $
                       cam2:{ndr:113L, exp:0.0, coadd:3}, $
                       cam3:{ndr:47L, exp:0.0, coadd:3}}], $
                     
                     [{ircam:0, $ ;off-disk, high-res mode
                       cam1:{ndr:20L, exp:0.0, coadd:1}, $
                       cam2:{ndr:20L, exp:0.0, coadd:1}, $
                       cam3:{ndr:20L, exp:0.0, coadd:1}}, $
                      {ircam:0, $ ;off-disk, mid-res mode
                       cam1:{ndr:10L, exp:0.0, coadd:1}, $
                       cam2:{ndr:10L, exp:0.0, coadd:1}, $
                       cam3:{ndr:10L, exp:0.0, coadd:1}}, $
                      {ircam:0, $ ;off-disk, wide field mode
                       cam1:{ndr:6L, exp:0.0, coadd:1}, $
                       cam2:{ndr:6L, exp:0.0, coadd:1}, $
                       cam3:{ndr:6L, exp:0.0, coadd:1}}]] }

     ;; calculate baseline exposure time
     input.cam1.exp=input.cam1.ndr/input.cam1.fr/input.cam1.duty[1]
     input.cam2.exp=input.cam2.ndr/input.cam2.fr/input.cam2.duty[1]
     input.cam3.exp=input.cam3.ndr/input.cam3.fr/input.cam3.duty[1]
     
     ;; calculate plate scale for each feed ["/um]
     input.feed.ps=20.6265 / input.tele.diam / input.feed.fr ;[arcsec/micron]
     
     ;; calculate total fiber array dimensions
     input.ifu.iput.sx=(input.ifu.fiber.x + 2.*input.ifu.fiber.c) * $
                       input.ifu.iput.nrib*input.ifu.fiber.nfbr[0] + $
                       input.ifu.iput.ribsep*(input.ifu.iput.nrib-1)
     input.ifu.iput.sy=(input.ifu.fiber.y + 2.*input.ifu.fiber.c) * $
                       input.ifu.fiber.nfbr[1]*input.ifu.iput.nstk + $
                       input.ifu.iput.stksep*(input.ifu.iput.nstk-1)

                       
     ;;------------------------------------------------------------------------
     ;; set up output buffers
     sbuf=replicate({phot:0., back:0., dark:0., bias:0., snr:0., eff:0., $
                     dx:0., dy_fib:0., dy_cam:0., diff:0., dl:0., sat:0}, 3)
  
     nbuf=replicate({phot:0., back:0., dark:0., read:0., quan:0., total:0.}, 3)
     
     wbuf=replicate({l0:0.D, dl:0.D, ran:[0.D,0.D], vel:0.D}, 3)

     specbuf1=fltarr(input.cam1.nx/input.spec.nslits/input.pol.nbeams)
     specbuf2=fltarr(input.cam2.nx/input.spec.nslits/input.pol.nbeams)
     specbuf3=fltarr(input.cam3.nx/input.spec.nslits/input.pol.nbeams)
     
     output={signal:sbuf, noise:nbuf, wave:wbuf, $
             target:{ondisk:0, robs:0., mu:0.}, $
             mosaic:{tilex:0., tiley:0., fovx:0., fovy:0.}, $
             time:{frame:0., tile:0., mosaic:0., obs:0.}, $
             data:{tile:0L, mosaic:0L, obs:0L, rate:0.}, $
             spectrum1:{i:specbuf1, isource:specbuf1, iscat:specbuf1, $
                        isky:specbuf1, icont:specbuf1, l:double(specbuf1)}, $
             spectrum2:{i:specbuf2, isource:specbuf2, iscat:specbuf2, $
                        isky:specbuf2, icont:specbuf2, l:double(specbuf2)}, $
             spectrum3:{i:specbuf3, isource:specbuf3, iscat:specbuf3, $
                        isky:specbuf3, icont:specbuf3, l:double(specbuf3)}}
  endelse


  ;; High precision wavelengths for all lines of interest
  lines=[{l0: 854.2090D, id:'Ca II'}, $   ;NIST wavelength
         {l0: 789.1874D, id:'Fe XI'}, $   ;CHIANTI wavelength
         {l0:1074.6181D, id:'Fe XIII'}, $ ;CHIANTI wavelength
         {l0:1082.7091D, id:'Si I'}, $    ;NIST wavelength
         {l0:1082.9091D, id:'He I'}, $    ;NIST wavelength
         {l0:1083.0250D, id:'He I'}, $    ;NIST wavelength
         {l0:1083.0340D, id:'He I'}, $    ;NIST wavelength
         {l0:1430.0836D, id:'Si X'}, $    ;NIST wavelength
         {l0:1564.5020D, id:'Fe I'}, $    ;NIST wavelength
         {l0:1564.8514D, id:'Fe I'}, $    ;NIST wavelength
         {l0:1565.2873D, id:'Fe I'}]      ;NIST wavelength


  ;; Restore the atlas
  ;; atlas: w (wavelength in nm), i (normalized intensity)
  restore, dir+'data/NSO_FTS_combined_atlas.sav'

  ;; Load results of radiometric analysis
  restore, dir+'data/dlnirsp_throughput.sav'
  
END
